hdf - haskell data flow
-----------------------

hdf embeds a typed data-flow language for audio signal processing in
[haskell](http://haskell.org/).

hdf generates [C](http://c2.com/cgi/wiki?CeeLanguage) code that requires either
`Dl` (from [sc3-rdu](http://rohandrape.net/?t=sc3-rdu)) or
`rju-dl` (from [rju](http://rohandrape.net/?t=rju)).

The
[Sc2](http://audiosynth.com/)
analog bubbles
[.scd](?t=hsc3-graphs&e=lib/scd/graph/jmcc-analog-bubbles.scd)
graph can be written:

~~~~
let o = lf_saw (mce2 8.0 7.23) 0.0 * 3.0 + 80.0
    m = lf_saw 0.4 0.0 * 24.0 + o
    s = sin_osc (midi_cps m) 0.0 * 0.04
in comb_n 0.4 s 0.2 4.0
~~~~

The data flow graph this generates is:

![](sw/hdf/svg/jmcc-analog-bubbles.svg)

<!-- The generated C-code (for `jack-dl`) is [c/gen/analog-bubbles.c](sw/hdf/c/gen/analog-bubbles.c). -->

The library of primitives is [c/hdf.h](?t=hdf&e=c/hdf.h),
the world state is either
[Dl.h](?t=sc3-rdu&e=cpp/Dl.h) or
[rju-dl.h](?t=rju&e=cmd/rju-dl.h).

Slightly more elaborate, the berlin 1977
[.scd](?t=hsc3-graphs&e=lib/scd/graph/jmcc-berlin-1977.scd)
graph can be written:

~~~~
let clock_rate = 9
    k_sec = k_seconds_to_frames 48000
    clock_time = 1 / clock_rate
    clock = impulse clock_rate 0 -- sequencer trigger
    tr = trigger clock
    note = sequ [55,60,63,62,60,67,63,58] tr -- midi note pattern sequencer
    tr_16 = pulse_divider_bool tr 16 0 -- divide tr by 16
    note_trs = sequ [-12,-7,-5,0,2,5] tr_16 + note -- transpose
    freq = midi_cps note_trs -- convert midi note to cycles per second
    env = decay2 clock (0.05 * clock_time) (2 * clock_time)
    amp = env * 0.1 + 0.02 -- amplitude envelope
    filt = env * (sin_osc 0.17 0 * 800) + 1400 -- filter frequency
    pw = sin_osc (mce2 0.08 0.09) 0 * 0.45 + 0.5 -- pulse width LFO(s)
    s = lf_pulse freq 0 pw * amp -- not bandlimited
in comb_n (k_sec 0.2) (rlpf s filt 0.15) (mce2 0.2 0.17) 1.5
~~~~

The data flow graph this generates is:

![](sw/hdf/svg/jmcc-berlin-1977.impl.svg)

<!-- The generated C-code (for `jack-dl`) is [c/gen/berlin-1977.c](sw/hdf/c/gen/berlin-1977.c). -->

hdf allows sub-graphs to be evaluated on `demand`.
The two `sequ` nodes above each introduce a demand sub-graph,
drawn in boxes in the picture,
and bracketed in `if ... then ... endif` clauses in the generated C-code.

<!-- These are from the small set of example [graphs][ix]. -->

hdf implements `text-dl` for testing.

- [hdf.md](?t=hdf&e=md/hdf.md)

<!-- IX: [HTML](http://rohandrape.net/?t=hdf&e=md/ix.md) -->

initial announcement:
[[local](?t=hdf&e=md/announce.md),
 [haskell-art](http://lurk.org/r/topic/3ACgZsz81UZR8SMCegAdNl)]

<!--
[gmane](http://article.gmane.org/gmane.comp.lang.haskell.art/1028)
[bham](http://www.listarc.bham.ac.uk/lists/sc-users/msg42057.html)
-->

© [rohan drape][rd], 2006-2024, [gpl]

[rd]: http://rohandrape.net/
[gpl]: http://gnu.org/copyleft/

* * *

```
$ make doctest
Examples: 21  Tried: 21  Errors: 0  Failures: 0
$
```
