# hdf

A counter is a first order `iir` at `+` with a unit delay.

    unit_delay 0 (iir1 0 (+) 1)

There is a hdf function to make a counter.

    counter 0 1

The graph drawing indicates two delay arcs (in red), one introduced by
`iir1`, the other by `unit_delay`.  Node colours indicate data type,
_boolean_ nodes are brown, _integer_ nodes orange, _float_ nodes blue,
_nil_ nodes black, _vector_ nodes purple, and _sink_ nodes grey.  Edge
colours indicate the wire type, normal edges are black, implicit edges
are red, recursion edges are orange (reader) and purple (writer).

![](sw/hdf/svg/counter.svg)

An alternative drawing can be made that includes the _read_ and
_write_ nodes that implement the recursion scheme, connected by an
_implicit_ edge.

![](sw/hdf/svg/counter.impl.svg)

A simpler drawing, uncoloured, showing inputs ports as indices on
labeled edges.

![](sw/hdf/svg/counter.impl.gr.svg)
