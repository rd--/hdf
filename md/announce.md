# [haskell art] [ANN] HDF

Rohan Drape\
Tue, 14 Oct 2014 23:22:59 -0700

Dear List,

HDF is simple compiler for writing uniform rate data flow graphs.

"Berlin 1977" in "HDF 2006".

~~~~
let clock_rate = 9
    clock_time = 1 / clock_rate
    clock = impulse clock_rate 0
    tr = trigger clock
    note = sequ 0 [55,60,63,62,60,67,63,58] tr
    tr_16 = pulse_divider tr 16 0
    note' = sequ 1 [-12,-7,-5,0,2,5] tr_16 + note
    freq = midi_cps note'
    env = decay2 clock (0.05 * clock_time) (2 * clock_time)
    amp = env * 0.1 + 0.02
    filt = env * (sin_osc 0.17 0 * 800) + 1400
    pw = sin_osc (mce2 0.08 0.09) 0 * 0.45 + 0.5
    s = lf_pulse freq 0 pw * amp
in out (comb_n [0,1] 0.2 (rlpf s filt 0.15) (mce2 0.2 0.17) 1.5)
~~~~

HDF generates C code.

HDF graphs can be dynamically loaded into a running SuperCollider unit
generator, or into a running JACK process.

HDF is announced (belatedly) because it may be a useful starting point for
writing either:

- a SuperCollider-like notation for FAUST
- a SuperCollider unit generator that dynamically loads FAUST graphs

Archive: <http://rd.slavepianos.org/t/hdf>\
RDL: <http://rohandrape.net/?t=sc3-rdu&e=help/lhs/rdl.help.lhs>

Regards,\
Rohan
