module Sound.Df.Gadt.Df.W32 where

-- * Fractional

instance Fractional (Df Float) where
  (/) = mk_binop "df_fdiv" (/)
  recip = mk_uop "df_recip" recip
  fromRational = K . fromRational

-- * Sc3

-- | If the magnitude of the input is very small or very large or +inf or NaN then output zero.
df_zap_gremlins_f32 :: Df Float -> Df Float
df_zap_gremlins_f32 = mk_p1 "df_zap_gremlinsf" Ll.float_t

-- * Cast

-- | Cast floating point to integer.
df_float_to_int32 :: Df Float -> Df Int32
df_float_to_int32 = mk_p1 "df_float_to_int32" Ll.int32_t

-- | Cast integer to floating point.
df_int32_to_float :: Df Int32 -> Df Float
df_int32_to_float = mk_p1 "df_int32_to_float" Ll.float_t

{- | Scale 'Int32' to (-1,1) normalised 'Float'.

> maxBound == (2147483647::Int32)
-}
i32_to_normal_f32 :: Df Int32 -> Df Float
i32_to_normal_f32 = (/ 2147483647) . df_int32_to_float

-- * RealFrac/Float

-- | ceilf(3)
df_ceilf :: Df Float -> Df Float
df_ceilf = mk_uop "df_ceilf" (fromInteger . ceiling)

-- | floorf(3)
df_floorf :: Df Float -> Df Float
df_floorf = mk_uop "df_floorf" (fromInteger . floor)

-- | lrintf(3), ie. round to nearest integer.
df_lrintf :: Df Float -> Df Int32
df_lrintf = mk_p1 "df_lrintf" Ll.int32_t

-- | roundf(3)
df_roundf :: Df Float -> Df Float
df_roundf = mk_uop "df_roundf" (fromInteger . round)
