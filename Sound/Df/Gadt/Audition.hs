-- | Interaction with @jack-dl@, @scsynth@ and @text-dl@.
module Sound.Df.Gadt.Audition where

import Sound.Osc.Core {- hosc -}

import qualified Sound.Df.Gadt.Df as Df
import qualified Sound.Df.Ll.Audition as Audition
import qualified Sound.Df.Ll.Cgen as Cgen
import qualified Sound.Df.Udf as Udf

-- | Transform 'Df.Df' to 'Cgen.Instructions'.
df_instructions :: Df.Df () -> Cgen.Instructions
df_instructions = Udf.udf_instructions . Df.df_lower

-- | Audition graph at @jack-dl@ after sending initialisation messages.
audition_rju :: [Message] -> Df.Df () -> IO ()
audition_rju is = Audition.audition_rju is . df_instructions

-- | Audition graph at @Sc3@ after sending initialisation messages.
audition_sc3 :: [Message] -> Df.Df () -> IO ()
audition_sc3 is = Audition.audition_sc3 is . df_instructions

-- | Audition graph at @text-dl@.
audition_text :: Int -> Df.Df () -> IO ()
audition_text nf = Audition.audition_text nf . df_instructions
