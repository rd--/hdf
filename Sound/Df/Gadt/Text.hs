-- | Text Io for typed Df structure (not implemented).
module Sound.Df.Gadt.Text where

import Data.Functor.Identity {- base -}

import qualified Text.Parsec as P {- parsec -}
import qualified Text.Parsec.Language as P {- parsec -}
import qualified Text.Parsec.String as P {- parsec -}
import qualified Text.Parsec.Token as P {- parsec -}

import Sound.Df.Ll.K

-- | A 'Char' parser with no user state.
type P a = P.GenParser Char () a

lexer :: P.GenTokenParser String () Identity
lexer = P.makeTokenParser P.haskellDef

-- | /p/ consuming any trailing separators.
lexeme :: P t -> P t
lexeme = P.lexeme lexer

type_univ :: [String]
type_univ = map (show . snd) df_type_table

-- | Run parser and report any error.
parse :: P t -> String -> t
parse p = either (\m -> error ("parse: " ++ show m)) id . P.parse p ""

{- | Parse type

>>> map (parse parse_type) ["Int64_t","Bool_t"]
[Int64_t,Bool_t]
-}
parse_type :: P Df_Type
parse_type = fmap read (lexeme (P.choice (map (P.try . P.string) type_univ)))

{- | Parse K

>>> map (parse parse_K) ["K Nil_t","K Double_t 0.5","K Int64_t 1"]
[(),0.5,1]
-}
parse_K :: P K_Ty
parse_K = do
  _ <- lexeme (P.char 'K')
  ty <- parse_type
  case ty of
    Nil_t -> return (N ())
    Double_t -> fmap F64 (P.float lexer)
    Int64_t -> fmap (I64 . read) (P.many1 P.digit)
    _ -> error "parse_K_ty"

{- | Parse P0

>>> parse parse_P0 "P0 df_sample_rate Double_t"
("df_sample_rate",Double_t)
-}
parse_P0 :: P (String, Df_Type)
parse_P0 = do
  _ <- lexeme (P.string "P0")
  nm <- P.identifier lexer
  ty <- parse_type
  return (nm, ty)
