-- | Buffer
module Sound.Df.Gadt.Buffer where

{- | Buffer delay.

> draw (buf_delay 0 0.0 0)
-}
buf_delay :: Df Int64 -> Df Double -> Df Int64 -> Df Double
buf_delay b s n =
  let wi = phasor n 0 1
      ri = clipr n (wi + 1)
  in Mrg (bufread b ri) [bufwrite b wi s]

{- | Non-interpolating comb filter.  Inputs are: /b/ = buffer index,
/i/ = input signal, /dl/ = delay time, /dc/ = decay time.

All times are in seconds.  The decay time is the time for the
echoes to decay by @60@ decibels. If this time is negative then the
feedback coefficient will be negative, thus emphasizing only odd
harmonics at an octave lower.

> draw (out1 (buf_comb_n 0 0.0 0.0 0.0))
-}
buf_comb_n :: Df Int64 -> Df Double -> Df Double -> Df Double -> Df Double
buf_comb_n b s dlt dct =
  let n = df_lrint (dlt * w_sample_rate)
      fb = calc_fb_from_dt dlt dct
      c i =
        let x = buf_delay b i n
        in split (s + (fb * x))
  in knotP 0 c
