module Sound.Df.Gadt.Event where

import Data.Int {- base -}

import Sound.Df.Gadt.Df {- hdf -}
import Sound.Df.Gadt.Ugen {- hdf -}

-- | (w/gate,x,y,z/force,orientation,radius-x,radius-y,pitch,pitch-x,pitch-y) ; #10
type Event = (Df Double, Df Double, Df Double, Df Double, Df Double, Df Double, Df Double, Df Double, Df Double, Df Double)

event_addr :: Df Int64 -> Df Int64 -> Event
event_addr k0 c =
  let f i = lag (kr (ctl1 (k0 + (c * 10) + i))) 0.01
  in (f 0, f 1, f 2, f 3, f 4, f 5, f 6, f 7, f 8, f 9)

eventvoicer_addr :: Df Int64 -> Df Int64 -> Int64 -> (Int64 -> Event -> Df Double) -> Df Double
eventvoicer_addr k0 c0 n f = Mce (map (\c -> f c (event_addr k0 (c0 + K c))) [0 .. n - 1])

eventvoicer :: Int64 -> (Int64 -> Event -> Df Double) -> Df Double
eventvoicer = eventvoicer_addr 13000 0
