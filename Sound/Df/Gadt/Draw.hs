-- | Graph drawing
module Sound.Df.Gadt.Draw where

import qualified Sound.Df.Gadt.Df as Gadt
import qualified Sound.Df.Ll as Ll
import qualified Sound.Df.Udf.Dot as Udf

-- | 'Udf.draw' of graph
draw :: Ll.K a => Gadt.Df a -> IO ()
draw = Udf.draw . Gadt.df_lower

-- | 'Udf.gr_draw' of graph
gr_draw :: Ll.K a => Gadt.Df a -> IO ()
gr_draw = Udf.gr_draw . Gadt.df_lower
