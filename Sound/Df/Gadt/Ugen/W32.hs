module Sound.Df.Gadt.Ugen.W32 where

-- * Noise

{- | 'Int32' linear congruential generator, hence signed modulo of @2^32@.
  Note that the state and all internal math is 32bit.

See <http://en.wikipedia.org/wiki/Linear_congruential_generator> for possible parameters.
-}
lcg_i32 :: Int32 -> Int32 -> Int32 -> Df Int32
lcg_i32 a c x0 =
  let tilde f g = rec_u x0 (\i -> let r = f i in (r, g r))
  in ((K c) +) `tilde` (* (K a))

-- | 'lcg_i32' 1103515245 12345, so in (minBound,maxBound).
lcg_i32_glibc :: Df RNG32 -> Df Int32
lcg_i32_glibc df =
  case df of
    K (RNG32 k) -> lcg_i32 1103515245 12345 k
    _ -> error "lcg_i32_glibc: non-constant seed?"

{- | 'abs' of 'lcg_i64_knuth', so in (0,maxBound).

> maxBound == (2147483647::Int32)
-}
rand_i32 :: Df RNG32 -> Df Int32
rand_i32 = abs . lcg_i32_glibc

-- | 'i32_to_normal_f32' of 'rand_i32', so in (0,1).
rand_f32 :: Df RNG32 -> Df Float
rand_f32 = i32_to_normal_f32 . rand_i32
