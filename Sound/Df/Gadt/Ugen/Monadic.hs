-- | Data flow node functions, or unit generators.
module Sound.Df.Gadt.Ugen.Monadic where

import Control.Monad {- base -}
import Data.Int {- base -}
import Data.Maybe {- base -}

import qualified Sound.Sc3.Common.Math as Math {- hsc3 -}
import Sound.Sc3.Common.Uid {- hsc3 -}
import qualified Sound.Sc3.Ugen.Hs as Hs {- hsc3 -}

import Sound.Df.Gadt.Df
import Sound.Df.Gadt.Ugen
import Sound.Df.Ll.K

-- | 'iir1_g' of 'rec_m'
iir1_m :: (K a, UId m) => a -> (Binary_Op (Df a)) -> Df a -> m (Df a)
iir1_m = iir1_g rec_m

{- | 'phasor_P_g' of 'iir1_m'

> draw =<< phasor_m 9.0 (4.5::Double) 0.5
> drawM (phasor_m 9 (4.5::Double) 0.5)
-}
phasor_m :: (K_Num a, UId m) => Df a -> a -> Df a -> m (Df a)
phasor_m = phasor_P_g iir1_m

-- * Array

-- | 'a_alloc_g' of 'df_vec_m'
a_alloc_m :: UId m => Int64 -> m (Df (Vec Double))
a_alloc_m = a_alloc_g df_vec_m

-- | Array delay.
a_delay_m :: UId m => Df (Vec Double) -> Df Double -> Df Int64 -> m (Df Double)
a_delay_m a s n = do
  wi <- phasor_m n 0 1
  return (a_delay_ph a s n wi)

-- | 'df_vec_m' of 'tbl_sin'
a_tbl_sin_m :: UId m => Int -> m (Df (Vec Double))
a_tbl_sin_m = df_vec_m . tbl_sin

-- * Osc

{- | 'tbl_phasor_g' of 'phasor_m'

> drawM (phasor 64.0 (0.0::Double) (Math.cps_to_incr k_sample_rate 64.0 330.0))
> drawM (tbl_phasor 64 0.0 330.0)
-}
tbl_phasor_m :: UId m => Int -> Double -> Df Double -> m (Df Double)
tbl_phasor_m = tbl_phasor_g phasor_m

-- | Table lookup oscillator. /ip/ is in (0,1).
a_osc_m :: UId m => Df (Vec Double) -> Df Double -> Double -> m (Df Double)
a_osc_m a f ip = do
  let z = fromMaybe 0 (df_tbl_size a)
  p <- tbl_phasor_m z ip f
  return (a_lerp a p)

-- * Filter constructors.

-- | Single sample delay with indicated initial value.
unit_delay_m :: (K a, UId m) => a -> Df a -> m (Df a)
unit_delay_m y0 s = rec_m y0 (\i -> (i, s))

-- | 'iir2_g' of 'rec_m'
iir2_m :: (K_Num a, UId m) => (Ternary_Op (Df a)) -> Df a -> m (Df a)
iir2_m = iir2_g rec_m

-- | Single place finite impulse response filter.
fir1_m :: UId m => (Binary_Op (Df Double)) -> Df Double -> m (Df Double)
fir1_m f i = do
  x1 <- unit_delay_m 0 i
  return (f i x1)

-- | Two place finite impulse response filter.
fir2_m :: UId m => (Ternary_Op (Df Double)) -> Df Double -> m (Df Double)
fir2_m f i = do
  x1 <- unit_delay_m 0.0 i
  x2 <- unit_delay_m 0.0 x1
  return (f i x1 x2)

-- | Ordinary biquad filter section.
biquad_m :: UId m => (Quinary_Op (Df Double)) -> Df Double -> m (Df Double)
biquad_m f i =
  rec_mM
    0.0
    ( liftM split
        . ( \y1 -> do
              x1 <- unit_delay_m 0.0 i
              x2 <- unit_delay_m 0.0 x1
              y2 <- unit_delay_m 0.0 y1
              return (f i x1 x2 y1 y2)
          )
    )

-- * Counter

{- | Counter from indicated initial value.

> draw =<< counter (0::Int64) 1
> drawM (counter (0.0::Double) 1.0)

> audition_text 10 . out1 =<< counter_m 0.0 1.0
-}
counter_m :: (K_Num a, UId m) => a -> Df a -> m (Df a)
counter_m y0 n = unit_delay_m y0 =<< iir1_m y0 (+) n

-- * Buffer

{- | Buffer delay.

> drawM (buf_delay 0 0.0 0)
-}
buf_delay_m :: UId m => Df Int64 -> Df Double -> Df Int64 -> m (Df Double)
buf_delay_m b s n = do
  wi <- phasor_m n 0 1
  let ri = clipr n (wi + 1)
  return (MRG (b_read b ri) [b_write b wi s])

{- | Non-interpolating comb filter.  Inputs are: /b/ = buffer index,
/i/ = input signal, /dl/ = delay time, /dc/ = decay time.

All times are in seconds.  The decay time is the time for the
echoes to decay by @60@ decibels. If this time is negative then the
feedback coefficient will be negative, thus emphasizing only odd
harmonics at an octave lower.

> drawM (fmap out1 (buf_comb_n 0 0.0 0.0 0.0))

Comb used as a resonator. The resonant fundamental is equal to
reciprocal of the delay time.

> import qualified Sound.Sc3 as S

> do {n <- white_noise_m
>    ;dt <- let f x = lin_exp (x + 2.0) 1.0 2.0 0.0001 0.01
>           in fmap f (lf_saw 0.1 0.0)
>    ;c <- buf_comb_n 0 (n * 0.1) dt 0.2
>    ;audition [Hs.b_alloc 0 48000 1] (out1 c)}

Comb used as an echo.

> do {i <- impulse 0.5 0.0
>    ;n <- white_noise_m
>    ;e <- decay (i * 0.5) 0.2
>    ;c <- buf_comb_n 0 (e * n) 0.2 3.0
>    ;audition [Hs.b_alloc 0 48000 1] (out1 c)}
-}
buf_comb_n_m :: UId m => Df Int64 -> Df Double -> Df Double -> Df Double -> m (Df Double)
buf_comb_n_m b s dlt dct = do
  let n = df_lrint (dlt * w_sample_rate)
      fb = calc_fb_from_dt dlt dct
      c i = do
        x <- buf_delay_m b i n
        return (split (s + (fb * x)))
  rec_mM 0.0 c

-- * Comb

-- | Array variant of 'buf_comb_n'.  Max delay time is in frames.
comb_n_m :: UId m => Int64 -> Df Double -> Df Double -> Df Double -> m (Df Double)
comb_n_m z s dlt dct = do
  a <- a_alloc_m z
  let n = df_lrint (dlt * w_sample_rate)
      fb = calc_fb_from_dt dlt dct
      c i = do
        x <- a_delay_m a i n
        return (split (s + (fb * x)))
  rec_mM 0.0 c

-- * Noise

lift_rng_1 :: UId m => (Df RNG64 -> b) -> m b
lift_rng_1 f = do
  i <- generateUId
  return (f (K (RNG64 (fromIntegral i))))

-- | White noise (-1,1).  Generates noise whose spectrum has equal  power at all frequencies.
white_noise_m :: UId m => m (Df Double)
white_noise_m = lift_rng_1 white_noise

-- | Brown noise (-1,1).  Generates noise whose spectrum falls off in power by 6 dB per octave.
brown_noise_m :: UId m => m (Df Double)
brown_noise_m = lift_rng_1 brown_noise

-- * Osc

-- | Sine oscillator.  Inputs are: /f/ = frequency (in cps), /ip/ = initial phase.
sin_osc_m :: UId m => Df Double -> Double -> m (Df Double)
sin_osc_m f ip = do
  p <- phasor_m two_pi ip (Math.cps_to_incr w_sample_rate two_pi f)
  return (sin p)

{- | Impulse oscillator (non band limited).
Outputs non band limited single sample impulses.
Inputs are: /f/ = frequency (in hertz), /ip/ = phase offset (0..1)
-}
impulse_m :: UId m => Df Double -> Double -> m (Df Double)
impulse_m f ip = do
  let i = Math.cps_to_incr w_sample_rate 1.0 f
  p <- phasor_m 1.0 ip i
  x1 <- unit_delay_m 0.0 p
  let s = (x1 `df_lt` 0.5) `df_and` (p `df_gte` 0.5)
  return (select2 s 1.0 0.0)

-- * LF Osc.

{- | Non-band limited sawtooth oscillator.  Output ranges from -1 to +1.
Inputs are: /f/ = frequency (in hertz), /ip/ = initial phase (0,2).
-}
lf_saw_m :: UId m => Df Double -> Double -> m (Df Double)
lf_saw_m f ip = do
  p <- phasor_m 2.0 ip (Math.cps_to_incr w_sample_rate 2.0 f)
  return (p - 1.0)

{- | Non-band-limited pulse oscillator. Outputs a high value of one
and a low value of zero. Inputs are: /f/ = frequency (in hertz),
/ip/ = initial phase (0,1), /w/ = pulse width duty cycle (0,1).
-}
lf_pulse_m :: UId m => Df Double -> Double -> Df Double -> m (Df Double)
lf_pulse_m f ip w = do
  p <- phasor_m 1.0 ip (Math.cps_to_incr w_sample_rate 1.0 f)
  return (select2 (p `df_gte` w) 0.0 1.0)

-- * Filters

-- | Two zero fixed midpass filter.
bpz2_m :: UId m => Df Double -> m (Df Double)
bpz2_m = fir2_m (\x _ x2 -> (x - x2) * 0.5)

-- | Two zero fixed midcut filter.
brz2_m :: UId m => Df Double -> m (Df Double)
brz2_m = fir2_m (\x _ x2 -> (x + x2) * 0.5)

-- | Two point average filter
lpz1_m :: UId m => Df Double -> m (Df Double)
lpz1_m = fir1_m (\x x1 -> (x + x1) * 0.5)

-- | Two zero fixed lowpass filter
lpz2_m :: UId m => Df Double -> m (Df Double)
lpz2_m = fir2_m (\x x1 x2 -> (x + (2.0 * x1) + x2) * 0.25)

-- | One pole filter.
one_pole_m :: UId m => Df Double -> Df Double -> m (Df Double)
one_pole_m i cf = iir1_m 0.0 (one_pole_f cf) i

-- | One zero filter.
one_zero_m :: UId m => Df Double -> Df Double -> m (Df Double)
one_zero_m i cf = fir1_m (one_zero_f cf) i

-- | Second order filter section.
sos_m :: UId m => Df Double -> Df Double -> Df Double -> Df Double -> Df Double -> Df Double -> m (Df Double)
sos_m i a0 a1 a2 b1 b2 = biquad_m (sos_f a0 a1 a2 b1 b2) i

{- | Resonant low pass filter. Inputs are: /i/ = input signal, /f/ =
frequency (hertz), /rq/ = reciprocal of Q (resonance).
-}
rlpf_m :: UId m => Df Double -> Df Double -> Df Double -> m (Df Double)
rlpf_m i f r = iir2_m (Hs.rlpf_f df_max (w_radians_per_sample, f, r)) i

-- * Triggers

{- | Sample and hold. Holds input signal value when triggered.  Inputs
are: /i/ = input signal, /t/ = trigger.
-}
latch_m :: (K_Num a, UId m) => Df a -> Df Bool -> m (Df a)
latch_m i t = iir1_m 0 (select2 t) i

-- * Decays

{- | Exponential decay. Inputs are: /i/ = input signal, /t/ = decay
time.  This is essentially the same as Integrator except that
instead of supplying the coefficient directly, it is caculated from
a 60 dB decay time. This is the time required for the integrator to
lose 99.9 % of its value or -60dB. This is useful for exponential
decaying envelopes triggered by impulses.
-}
decay_m :: UId m => Df Double -> Df Double -> m (Df Double)
decay_m i dt = iir1_m 0.0 (decay_f dt) i

-- | Exponential decay (equivalent to @decay dcy - decay atk@).
decay2_m :: UId m => Df Double -> Df Double -> Df Double -> m (Df Double)
decay2_m i atk dcy = liftM2 (-) (decay_m i dcy) (decay_m i atk)

-- * Delays

-- | Single sample delay.
delay1_m :: (K_Num a, UId m) => Df a -> m (Df a)
delay1_m = iir1_m 0 (\_ y1 -> y1)

-- | Two sample delay.
delay2_m :: (K_Num a, UId m) => Df a -> m (Df a)
delay2_m = iir2_m (\_ _ y2 -> y2)

-- * Lags

-- | Simple averaging filter.  Inputs are: /i/ = input signal, /t/ = lag time.
lag_m :: UId m => Df Double -> Df Double -> m (Df Double)
lag_m i t = iir1_m 0 (Hs.lag_f w_sample_rate t) i

-- | Nested lag filter.
lag2_m :: UId m => Df Double -> Df Double -> m (Df Double)
lag2_m i t = do
  a <- lag_m i t
  lag_m a t

-- | Twice nested lag filter.
lag3_m :: UId m => Df Double -> Df Double -> m (Df Double)
lag3_m i t = do
  a <- lag_m i t
  b <- lag_m a t
  lag_m b t

-- * Rec

-- | Variant of 'rec_m' with monadic action in backward arc.
rec_mM :: (K1 a, UId.UId m) => a -> (Df a -> m (Df b, Df a)) -> m (Df b)
rec_mM i f = do
  n <- UId.generateUId
  let t = Ll.df_typerep_type (typeOf i)
      r_r = R (Udf.R_Id n) t (Left i)
  r <- f r_r
  return (R (Udf.R_Id n) t (Right r))
