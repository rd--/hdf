{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}

-- | Typed data flow nodes.
module Sound.Df.Gadt.Df where

import Control.Monad {- base -}
import Data.Bits {- base -}
import Data.Fixed {- base -}
import Data.Int {- base -}
import Data.List {- base -}
import Data.Typeable {- base -}

import Data.List.Split {- split -}

import qualified Sound.Sc3.Common.Unsafe as Unsafe {- hsc3 -}

import qualified Sound.Df.Ll as Ll
import qualified Sound.Df.Udf as Udf

-- * Df

-- | Tuple constraint types.
type K1 a = Ll.K a

type K2 a b = (Ll.K a, Ll.K b)
type K3 a b c = (Ll.K a, Ll.K b, Ll.K c)
type K4 a b c d = (Ll.K a, Ll.K b, Ll.K c, Ll.K d)

{- | Data flow node.

Notes:

The Mce implementation is not excellent, perhaps the model could be simplified.
The Udf layer does not know about Mce but does know about Dmd.
Dmd requires a proper sub-graph model, i.e. in the type.
If the model were a simple interpreter could Mce be implemented once only (at Apply) and could the sub-graph requirement for Dmd be implemented by evaluating the Dmd closure with an initial environment so that it cannot refer to existing values.

K = constant (including arrays),
R = recursion
Pn = primitive,
Dmd = demand,
Ref = reference
Mce = multiple-channel-expansion,
Mrg = multiple-root-graph.
-}
data Df a where
  K :: K1 a => a -> Df a
  MemRd :: K1 a => Ll.MemId -> a -> Df a
  MemWr :: K1 a => Ll.MemId -> a -> Df a -> Df ()
  P0 :: K1 a => String -> Ll.Df_Type -> Df a
  P1 :: K2 a b => String -> Ll.Df_Type -> Df a -> Df b
  P2 :: K3 a b c => String -> Ll.Df_Type -> Df a -> Df b -> Df c
  P3 :: K4 a b c d => String -> Ll.Df_Type -> Df a -> Df b -> Df c -> Df d
  Dmd :: K1 a => a -> Df Bool -> Df a -> Df a
  Ref :: K1 a => Df a -> Int -> Df a
  Mce :: [Df a] -> Df a
  Mrg :: K2 a b => Df a -> [Df b] -> Df a

deriving instance Show a => Show (Df a)

assert_all_equal :: Eq t => String -> [t] -> t
assert_all_equal msg x = if length x > 0 && length (nub x) == 1 then head x else error msg

{- | Typeable instance for 'Df'.

>>> df_type_of (K (undefined::Int64))
Int64_t

>>> df_type_of (K (undefined::Double))
Double_t

>>> df_type_of (K (undefined::Ll.Vec Double))
Vec_Double_t

>>> df_type_of (0::Df Int64)
Int64_t

>>> df_type_of (0.0::Df Double)
Double_t
-}
df_type_of :: K1 a => Df a -> Ll.Df_Type
df_type_of df =
  case df of
    K k -> Ll.df_typerep_type (typeOf k)
    MemRd _ x -> Ll.df_typerep_type (typeOf x)
    MemWr _ _ _ -> Ll.Nil_t
    P0 _ t -> t
    P1 _ t _ -> t
    P2 _ t _ _ -> t
    P3 _ t _ _ _ -> t
    Dmd _ _ rhs -> df_type_of rhs
    Ref src _ -> df_type_of src
    Mce l -> case l of
      [] -> error "df_type_of: Mce []"
      _ -> assert_all_equal "df_type_of: mce" (map df_type_of l) -- ?
    Mrg n _ -> df_type_of n

-- | Name of primitive if 'Df' is 'P0' or 'P1' etc.
df_primitive :: Df a -> Maybe String
df_primitive df =
  case df of
    P0 nm _ -> Just nm
    P1 nm _ _ -> Just nm
    P2 nm _ _ _ -> Just nm
    P3 nm _ _ _ _ -> Just nm
    _ -> Nothing

df_k_err :: Df t -> t
df_k_err df =
  case df of
    K k -> k
    _ -> error "df_k"

-- | Unpack constant
df_k_mce_err :: Show a => Df a -> Either a [a]
df_k_mce_err df =
  case df of
    K k -> Left k
    Mce m -> Right (map df_k_err m)
    _ -> error ("df_k_mce: " ++ show df)

-- * Mrg

-- | Multiple root graph (alias for Mrg).
mrg :: K1 a => Df a -> [Df ()] -> Df a
mrg = Mrg

-- * Df Vec

-- | 'Df' 'Vec' constructor.
df_vec_id :: Ll.VecId -> [Double] -> Df (Ll.Vec Double)
df_vec_id k v = K (Ll.Vec k (length v) v)

-- | Monadic 'Df' 'Vec' constructor.
df_vec_m :: [Double] -> IO (Df (Ll.Vec Double))
df_vec_m = Ll.liftVec1 df_vec_id

df_vec :: [Double] -> Df (Ll.Vec Double)
df_vec = Unsafe.liftUnsafe1 df_vec_m

-- | 'Df' 'Vec' size.
df_vec_size :: Df (Ll.Vec a) -> Maybe Int
df_vec_size df =
  case df of
    K (Ll.Vec _ n _) -> Just n
    _ -> Nothing

-- | 'df_vec_size' variant, tables have a guard point.
df_tbl_size :: Df (Ll.Tbl a) -> Maybe Int
df_tbl_size = fmap (+ (-1)) . df_vec_size

-- * Operator types

-- | Unary operator.
type Unary_Op a = a -> a

-- | Binary operator.
type Binary_Op a = a -> a -> a

-- | Ternary operator.t
type Ternary_Op a = a -> a -> a -> a

-- | Quaternary operator.
type Quaternary_Op a = a -> a -> a -> a -> a

-- | Quinary operator.
type Quinary_Op a = a -> a -> a -> a -> a -> a

-- | Senary operator.
type Senary_Op a = a -> a -> a -> a -> a -> a -> a

-- * Function types

-- | Binary function.
type Binary_Fn i o = i -> i -> o

-- * Mce

unmce :: Df t -> [Df t]
unmce n =
  case n of
    Mce l -> l
    Mrg l r -> case unmce l of
      [] -> error "unmce: Mrg?"
      h : t -> Mrg h r : t
    _ -> [n]

-- | Mce predicate, sees into Mrg.
is_mce :: Df t -> Bool
is_mce n =
  case n of
    Mce _ -> True
    Mrg lhs _ -> is_mce lhs
    _ -> False

-- | Mce degree, sees into Mrg.
mce_degree :: Df t -> Int
mce_degree n =
  case n of
    Mce l -> length l
    Mrg l _ -> mce_degree l
    _ -> 1

-- | Mce extension, sees into Mrg, will not reduce.
mce_extend :: Int -> Df t -> [Df t]
mce_extend k n =
  if k < mce_degree n
    then error "mce_extend: REDUCE?"
    else case n of
      Dmd _ _ _ -> error "mce_extend: Dmd"
      Mce l -> take k (cycle l)
      Mrg lhs rhs -> case mce_extend k lhs of
        x0 : x -> Mrg x0 rhs : x
        _ -> error "mce_extend: mrg?"
      _ -> replicate k n

mce1 :: Df a -> Df a
mce1 p = Mce [p]

mce2 :: Df a -> Df a -> Df a
mce2 p q = Mce [p, q]

mce3 :: Df a -> Df a -> Df a -> Df a
mce3 p q r = Mce [p, q, r]

mce4 :: Df a -> Df a -> Df a -> Df a -> Df a
mce4 p q r s = Mce [p, q, r, s]

mce5 :: Df a -> Df a -> Df a -> Df a -> Df a -> Df a
mce5 p q r s t = Mce [p, q, r, s, t]

mce6 :: Df a -> Df a -> Df a -> Df a -> Df a -> Df a -> Df a
mce6 p q r s t u = Mce [p, q, r, s, t, u]

-- | 'sum' of 'unmce'
mix :: Ll.K_Num t => Df t -> Df t
mix = sum . unmce

mixto :: Ll.K_Num a => Int -> Df a -> Df a
mixto n = Mce . map sum . chunksOf n . unmce

unmce2 :: Show t => Df t -> (Df t, Df t)
unmce2 n =
  case unmce n of
    [p, q] -> (p, q)
    _ -> error ("unmce2: " ++ show n)

lift_mce :: (Df a -> Df b) -> Df a -> Df b
lift_mce f p =
  case p of
    Mce l -> Mce (map (lift_mce f) l)
    _ -> f p

lift_mce2 :: (Df a -> Df b -> Df c) -> Df a -> Df b -> Df c
lift_mce2 f p q =
  if is_mce p || is_mce q
    then
      let k = max (mce_degree p) (mce_degree q)
      in Mce (zipWith (lift_mce2 f) (mce_extend k p) (mce_extend k q))
    else f p q

max3 :: Ord a => a -> a -> a -> a
max3 p q r = max p (max q r)

mce_extend3 :: Df a -> Df b -> Df c -> (Int, [Df a], [Df b], [Df c])
mce_extend3 p q r =
  let k = max3 (mce_degree p) (mce_degree q) (mce_degree r)
  in (k, mce_extend k p, mce_extend k q, mce_extend k r)

lift_mce3 :: (Df a -> Df b -> Df c -> Df d) -> Df a -> Df b -> Df c -> Df d
lift_mce3 f p q r =
  if is_mce p || is_mce q || is_mce r
    then
      let (_, p', q', r') = mce_extend3 p q r
      in Mce (zipWith3 (lift_mce3 f) p' q' r')
    else f p q r

max4 :: Ord a => a -> a -> a -> a -> a
max4 p q r s = max p (max3 q r s)

mce_extend4 :: Df a -> Df b -> Df c -> Df d -> ([Df a], [Df b], [Df c], [Df d])
mce_extend4 p q r s =
  let k = max4 (mce_degree p) (mce_degree q) (mce_degree r) (mce_degree s)
  in (mce_extend k p, mce_extend k q, mce_extend k r, mce_extend k s)

lift_mce4 :: (Df a -> Df b -> Df c -> Df d -> Df e) -> Df a -> Df b -> Df c -> Df d -> Df e
lift_mce4 f p q r s =
  if is_mce p || is_mce q || is_mce r || is_mce s
    then
      let (p', q', r', s') = mce_extend4 p q r s
      in Mce (zipWith4 (lift_mce4 f) p' q' r' s')
    else f p q r s

-- * Demand

{- | Arguments are an initial value, the trigger and the demand sub-graph.
     The initial value must be constant and will be copied for Mce sub-graphs.
     Mce sub-graphs must be of equal type.
     The trigger must be non-Mce.
-}
demand :: K1 a => Df a -> Df Bool -> Df a -> Df a
demand def tr gr =
  let dmd = Dmd (df_k_err def) tr gr
  in if is_mce gr
      then Mce (map (\i -> Ref dmd i) [0 .. mce_degree gr - 1])
      else Ref dmd 0

-- * Primitive constructors

-- | 'lift_mce' of 'P1'.
mk_p1 :: K2 a b => String -> Ll.Df_Type -> Df a -> Df b
mk_p1 nm ty = lift_mce (P1 nm ty)

-- | Unary operator.
mk_uop :: K1 a => String -> (a -> a) -> Unary_Op (Df a)
mk_uop nm k_f p =
  case p of
    K x -> K (k_f x)
    _ -> mk_p1 nm (df_type_of p) p

-- | 'lift_mce2' of 'P2'.
mk_p2 :: K3 a b c => String -> Ll.Df_Type -> Df a -> Df b -> Df c
mk_p2 nm ty = lift_mce2 (P2 nm ty)

-- | Binary operator.
mk_binop :: K1 a => String -> (a -> a -> a) -> Binary_Op (Df a)
mk_binop nm k_f p q =
  case (p, q) of
    (K x, K y) -> K (k_f x y)
    _ -> mk_p2 nm (df_type_of p) p q

-- | 'lift_mce3' of 'P3'.
mk_p3 :: K4 a b c d => String -> Ll.Df_Type -> Df a -> Df b -> Df c -> Df d
mk_p3 nm ty = lift_mce3 (P3 nm ty)

-- | Binary operator.
mk_ternaryop :: K1 a => String -> Ternary_Op (Df a)
mk_ternaryop nm p q r = mk_p3 nm (df_type_of p) p q r

-- | 'Df' multiply and add.
df_mul_add :: Ll.K_Num a => Df a -> Df a -> Df a -> Df a
df_mul_add = mk_ternaryop "df_mul_add"

{- | Optimising addition primitive.
  If either input is a multiplier node, unfold to a multiplier-add node.

>>> df_add_optimise (2 * 3) (4::Df Int64)
K 10

>>> df_add_optimise (2::Df Int64) (3 * 4)
K 14
-}
df_add_optimise :: Ll.K_Num a => Df a -> Df a -> Df a
df_add_optimise p q =
  case (p, q) of
    (P2 "df_mul" t l r, _) -> mk_p3 "df_mul_add" t l r q
    (_, P2 "df_mul" t l r) -> mk_p3 "df_mul_add" t l r p
    _ -> mk_binop "df_add" (+) p q

instance Ll.K_Num a => Num (Df a) where
  (+) = df_add_optimise
  (*) = mk_binop "df_mul" (*)
  (-) = mk_binop "df_sub" (-)
  negate = mk_uop "df_negate" negate
  abs = mk_uop "df_abs" abs
  signum = mk_uop "df_signum" signum
  fromInteger = K . fromInteger

-- | 'div', except that Integral requires Real and Enum and they are mostly not applicable.
df_div :: Df Int64 -> Df Int64 -> Df Int64
df_div = mk_binop "df_idiv" div

-- | 'mod', except that Integral requires Real and Enum and they are mostly not applicable.
df_mod :: Df Int64 -> Df Int64 -> Df Int64
df_mod = mk_binop "df_imod" mod

instance Fractional (Df Double) where
  (/) = mk_binop "df_fdiv" (/)
  recip = mk_uop "df_recip" recip
  fromRational = K . fromRational

{- | https://www.musicdsp.org/en/latest/Other/238-rational-tanh-approximation.html
  c.f. https://mathr.co.uk/blog/2017-09-06_approximating_hyperbolic_tangent.html
-}
rational_tanh_noclip :: Fractional a => a -> a
rational_tanh_noclip x = x * (27 + x * x) / (27 + 9 * x * x)

instance Floating (Df Double) where
  pi = K pi
  exp = mk_uop "df_exp" exp
  sqrt = mk_uop "df_sqrt" sqrt
  log = mk_uop "df_log" log
  (**) = mk_binop "df_pow" (**)
  logBase = error "Floating: partial?"
  sin = mk_uop "df_sin" sin
  tan = mk_uop "df_tan" tan
  cos = mk_uop "df_cos" cos
  asin = undefined
  atan = undefined
  acos = undefined
  sinh = undefined
  tanh = rational_tanh_noclip
  cosh = undefined
  asinh = undefined
  atanh = undefined
  acosh = undefined

-- * Bits

-- | "Data.Bits" @.&.@.
df_bw_and :: Df Int64 -> Df Int64 -> Df Int64
df_bw_and = mk_binop "df_bw_and" (.&.)

-- | "Data.Bits" @.|.@.
df_bw_or :: Df Int64 -> Df Int64 -> Df Int64
df_bw_or = mk_binop "df_bw_or" (.|.)

-- | "Data.Bits" @complement@.
df_bw_not :: Df Int64 -> Df Int64
df_bw_not = mk_uop "df_bw_not" complement

-- | "Data.Bits" @testBit@.
df_bw_test_bit :: Df Int64 -> Df Int64 -> Df Bool
df_bw_test_bit = mk_p2 "df_bw_test_bit" Ll.Bool_t

-- * Ord

-- | '==', equal to.  Constraint should be K_Atom.
df_eq :: Ll.K_Ord a => Df a -> Df a -> Df Bool
df_eq = mk_p2 "df_eq" Ll.Bool_t

-- | Given epsilon, test for approximate equality.
df_eq_approx :: Df Double -> Df Double -> Df Double -> Df Bool
df_eq_approx = mk_p3 "df_eq_approx" Ll.Bool_t

-- | '<', less than.
df_lt :: Ll.K_Ord a => Df a -> Df a -> Df Bool
df_lt = mk_p2 "df_lt" Ll.Bool_t

-- | '>=', greater than or equal to.
df_gte :: Ll.K_Ord a => Df a -> Df a -> Df Bool
df_gte = mk_p2 "df_gte" Ll.Bool_t

-- | '>', greater than.
df_gt :: Ll.K_Ord a => Df a -> Df a -> Df Bool
df_gt = mk_p2 "df_gt" Ll.Bool_t

-- | '<=', less than or equal to.
df_lte :: Ll.K_Ord a => Df a -> Df a -> Df Bool
df_lte = mk_p2 "df_lte" Ll.Bool_t

-- | 'max', select maximum.
df_max :: Ll.K_Ord a => Df a -> Df a -> Df a
df_max = mk_binop "df_max" max

-- | 'min', select minimum.
df_min :: Ll.K_Ord a => Df a -> Df a -> Df a
df_min = mk_binop "df_min" min

-- * Sc3

-- | If the magnitude of the input is very small or very large or +inf or NaN then output zero.
df_zap_gremlins :: Df Double -> Df Double
df_zap_gremlins = mk_p1 "df_zap_gremlins" Ll.Double_t

-- * Cast

-- | Cast floating point to integer.
df_double_to_int64 :: Df Double -> Df Int64
df_double_to_int64 = mk_p1 "df_double_to_int64" Ll.Int64_t

-- | Cast integer to floating point.
df_int64_to_double :: Df Int64 -> Df Double
df_int64_to_double = mk_p1 "df_int64_to_double" Ll.Double_t

{- | Scale 'Int64' to (-1,1) normalised 'Double'.

>>> maxBound :: Int64
9223372036854775807
-}
i64_to_normal_f64 :: Df Int64 -> Df Double
i64_to_normal_f64 = (/ 9223372036854775807) . df_int64_to_double

-- * Real

-- | Floating point modulo, ie. "Data.Fixed" 'mod''.
df_fmod :: Binary_Op (Df Double)
df_fmod = mk_binop "df_fmod" mod'

-- * RealFrac

-- | ceil(3)
df_ceil :: Df Double -> Df Double
df_ceil = mk_uop "df_ceil" (fromInteger . ceiling)

-- | floor(3)
df_floor :: Df Double -> Df Double
df_floor = mk_uop "df_floor" (fromInteger . floor)

-- | lrint(3), ie. round to nearest integer.
df_lrint :: Df Double -> Df Int64
df_lrint = mk_p1 "df_lrint" Ll.Int64_t

-- | round(3)
df_round :: Df Double -> Df Double
df_round = mk_uop "df_round" (fromInteger . round)

-- * Backward arcs

{-

This is intricate.
The key relation is between y0 (the input to the knot function) and rev (the feedbackward result of the knot function).
The knot function cannot Mce expand on it's input.
(This would imply increasing the graph size infinitely.)
The knot input can be Mce but the feedbackward output must have the same degree.

-}

knotPId :: (K1 a, K1 b) => Ll.MemId -> a -> (Df a -> (Df b, Df a)) -> Df b
knotPId memid y0 f =
  let rd = MemRd memid y0
      (fwd, rev) = f rd
      wr = MemWr memid y0 rev
  in if is_mce rev
      then error "knotPId: function mce expands?"
      else Mrg fwd [wr]

knotPM :: (K1 a, K1 b) => a -> (Df a -> (Df b, Df a)) -> IO (Df b)
knotPM y0 f = do
  memid <- Ll.nextMemId
  return (knotPId memid y0 f)

knotP :: (K1 a, K1 b) => a -> (Df a -> (Df b, Df a)) -> Df b
knotP = Unsafe.liftUnsafe2 knotPM

-- * Mce Knot

memIdFor :: Show t => Df t -> IO (Either Ll.MemId [Ll.MemId])
memIdFor y0 =
  case df_k_mce_err y0 of
    Left _ -> fmap Left Ll.nextMemId
    Right x -> fmap Right (replicateM (length x) Ll.nextMemId)

-- | Mce is not allowed at Udf so it must not be stored in MemWr:rd or Mrg:rhs
knotMceId :: (K1 a, K1 b) => [Ll.MemId] -> [a] -> (Df a -> (Df b, Df a)) -> Df b
knotMceId memid y0 f =
  let rd = zipWith MemRd memid y0
      (fwd, rev) = f (Mce rd)
      wr = zipWith3 (\z y x -> MemWr z y x) memid y0 (unmce rev)
  in if length y0 /= mce_degree rev
      then error "knotMceId: Mce disagree"
      else Mrg fwd wr

knotMceM :: (K1 a, K1 b) => Df a -> (Df a -> (Df b, Df a)) -> IO (Df b)
knotMceM y0 f = do
  memid <- memIdFor y0
  case memid of
    Left m0 -> return (knotPId m0 (df_k_err y0) f)
    Right mSeq -> return (knotMceId mSeq (map df_k_err (unmce y0)) f)

knot :: (K1 a, K1 b) => Df a -> (Df a -> (Df b, Df a)) -> Df b
knot = Unsafe.liftUnsafe2 knotMceM

-- * Primitives

-- | Single channel input.
in1 :: Df Int64 -> Df Double
in1 = mk_p1 "df_in1" Ll.Double_t

-- | Single channel output.
out1 :: Df Int64 -> Df Double -> Df ()
out1 = mk_p2 "df_out1" Ll.Nil_t

-- | Two channel output (channels c & c+1).
out2 :: Df Int64 -> Df Double -> Df Double -> Df ()
out2 = mk_p3 "df_out2" Ll.Nil_t

{-
-- | Two channel output (channels c & c+1).
out3 :: Df Int64 -> Df Double -> Df Double -> Df Double -> Df ()
out3 = mk_p4 "df_out3" Ll.Nil_t

-- | Two channel output (channels c & c+1).
out4 :: Df Int64 -> Df Double -> Df Double -> Df Double -> Df Double -> Df ()
out4 = mk_p5 "df_out4" Ll.Nil_t
-}

-- | Mce collapsing output.
out :: Df Int64 -> Df Double -> Df ()
out c n =
  case n of
    Mce [p] -> out1 c p
    Mce [p, q] -> out2 c p q
    Mce _ -> error "out: Mce"
    _ -> out1 c n

-- | Single control input.
ctl1 :: Df Int64 -> Df Double
ctl1 = mk_p1 "df_ctl1" Ll.Double_t

-- | Logical '&&'.
df_and :: Df Bool -> Df Bool -> Df Bool
df_and = mk_binop "df_and" (&&)

-- | Logical '||'.
df_or :: Df Bool -> Df Bool -> Df Bool
df_or = mk_binop "df_or" (||)

-- | Logical 'not'.
df_not :: Df Bool -> Df Bool
df_not = mk_uop "df_not" not

-- | If /p/ then /q/ else /r/.
select2 :: K1 a => Df Bool -> Df a -> Df a -> Df a
select2 p q = mk_p3 "df_select2" (df_type_of q) p q

-- | Operating sample rate.
w_sample_rate :: Df Double
w_sample_rate = P0 "df_sample_rate" Ll.Double_t

-- | World random number generator.
w_frand :: Df Double -> Df Double -> Df Double
w_frand = P2 "df_frand" Ll.Double_t

-- | Compile time sample rate (currently fixed)
c_sample_rate :: Num n => n
c_sample_rate = 48000

{-
-- | Number of frames in current processing block.
w_block_nframes :: Df Int64
w_block_nframes = P0 "df_block_nframes" Ll.Int64_t

-- | 'True' at first frame of each processing block.
w_block_edge :: Df Bool
w_block_edge = P0 "df_block_edge" Ll.Bool_t
-}

-- | Read from buffer /p/ at index /q/.
bufread :: Df Int64 -> Df Int64 -> Df Double
bufread = mk_p2 "df_buf_read" Ll.Double_t

-- | Write into buffer /p/ at index /q/ the value /r/.
bufwrite :: Df Int64 -> Df Int64 -> Df Double -> Df ()
bufwrite = mk_p3 "df_buf_write" Ll.Nil_t

-- | Read from vector at index ; vec->index->value
vecread :: Df (Ll.Vec Double) -> Df Int64 -> Df Double
vecread = mk_p2 "df_vec_read" Ll.Double_t

-- | Write to vector at index ; vec->index->value->()
vecwrite :: Df (Ll.Vec Double) -> Df Int64 -> Df Double -> Df ()
vecwrite = mk_p3 "df_vec_write" Ll.Nil_t

-- * Untyped

-- | Transform typed 'Df' to un-typed 'Udf'.
df_erase :: K1 a => Df a -> Udf.Udf
df_erase n =
  case n of
    K i -> Udf.Udf_K (Ll.to_k i)
    MemRd z x -> Udf.Udf_MemRd z (Ll.to_k x)
    MemWr z x y -> Udf.Udf_MemWr z (Ll.to_k x) (df_erase y)
    P0 nm t -> Udf.Udf_P nm t []
    P1 nm t i -> Udf.Udf_P nm t [df_erase i]
    P2 nm t i j -> Udf.Udf_P nm t [df_erase i, df_erase j]
    P3 nm t i j k -> Udf.Udf_P nm t [df_erase i, df_erase j, df_erase k]
    Mce _ -> error ("df_erase: Mce: " ++ show n)
    Dmd def i j -> Udf.Udf_Dmd (Ll.to_k def) (df_erase i) (map df_erase (unmce j))
    Ref src ix -> Udf.Udf_Ref (df_erase src) ix
    Mrg i j -> Udf.Udf_Mrg (df_erase i) (map df_erase j)

-- | Transform Mce at root of graph to Mrg.
df_prepare_root :: K1 t => Df t -> Df t
df_prepare_root u =
  case u of
    Mce (x : xs) -> Mrg x xs
    _ -> u

-- | Lower typed Df to Udf.
df_lower :: K1 t => Df t -> Udf.Udf
df_lower = df_erase . df_prepare_root

-- * Rng

liftRng0 :: (Df Ll.Rng64 -> r) -> IO r
liftRng0 f = do
  r <- Ll.generateRng64
  return (f (K r))

liftRng1 :: (Df Ll.Rng64 -> a -> r) -> a -> IO r
liftRng1 f a = do
  r <- Ll.generateRng64
  return (f (K r) a)

liftRng2 :: (Df Ll.Rng64 -> a -> b -> r) -> a -> b -> IO r
liftRng2 f a b = do
  r <- Ll.generateRng64
  return (f (K r) a b)

liftRng3 :: (Df Ll.Rng64 -> a -> b -> c -> r) -> a -> b -> c -> IO r
liftRng3 f a b c = do
  r <- Ll.generateRng64
  return (f (K r) a b c)
