{-# LANGUAGE GADTs #-}

-- | Data flow node funcions, or unit generators.
module Sound.Df.Gadt.Ugen where

import Data.Int {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}

import qualified Sound.Sc3.Common.Math as Math {- hsc3 -}
import qualified Sound.Sc3.Common.Math.Filter as Filter {- hsc3 -}
import qualified Sound.Sc3.Common.Uid as Uid {- hsc3 -}
import qualified Sound.Sc3.Common.Unsafe as Unsafe {- hsc3 -}
import qualified Sound.Sc3.Ugen.Hs as Hs {- hsc3 -}

import Sound.Df.Gadt.Df {- hdf -}
import Sound.Df.Ll.K {- hdf -}

-- * Tuples

{- | Duplicate a value into a tuple.

>>> split 1
(1,1)
-}
split :: a -> (a, a)
split p = (p, p)

{- | Reversed tuple constructor, (ie. @flip (,)@)

>>> swap 2 1
(1,2)
-}
swap :: a -> b -> (b, a)
swap = flip (,)

-- * Math

twopi :: Floating a => a
twopi = Math.two_pi

midicps :: Floating a => a -> a
midicps = Math.midi_to_cps

muladd :: Num a => a -> a -> a -> a
muladd = Math.mul_add

{- | Calculate feedback multipler in comb & all-pass filter circuit given /delay/
and /decay/ times.

>>> calc_fb_from_dt 0.2 3.0
0.6309573444801932
-}
calc_fb_from_dt :: Floating a => a -> a -> a
calc_fb_from_dt delayt decayt = exp ((log 0.001 * delayt) / decayt)

-- | Map from one linear range to another linear range.
linlin :: Fractional a => a -> a -> a -> a -> a -> a
linlin = Math.sc3_linlin

-- | Exponential range conversion.
linexp :: Floating a => a -> a -> a -> a -> a -> a
linexp = Math.lin_exp

{- | Constrain p in (-q,q).

>>> let r = -10 : -10 : [-10,-9 .. 10] ++ [10,10]
>>> map (flip clip2 10) [-12,-11 .. 12] == r
True
-}
clip2 :: (Num a, Ord a) => a -> a -> a
clip2 p q = min q (max p (negate q))

-- | Sc3 linear pan.
linPan2 :: Df Double -> Df Double -> Df Double -> Df Double
linPan2 s p a = uncurry mce2 (Math.lin_pan2 (s * a) p)

-- | Sc3 equal-power pan.
pan2 :: Df Double -> Df Double -> Df Double -> Df Double
pan2 s p a = uncurry mce2 (Math.eq_pan2 (s * a) p)

-- | Sc3 distort operator.
distort :: Fractional n => n -> n
distort = Math.sc3_distort

-- | Sc3 softClip operator.
softclip :: Df Double -> Df Double
softclip x = select2 (abs x `df_lte` 0.5) x ((abs x - 0.25) / x)

-- | Sc3 cubed operator.
cubed :: Num a => a -> a
cubed x = x * x * x

-- * Environment

{- | Calculate n-frames given sample-rate and time in seconds.

>>> k_seconds_to_frames 48000 0.5
24000
-}
k_seconds_to_frames :: Int64 -> Double -> Int64
k_seconds_to_frames sr tm = floor (fromIntegral sr * tm)

c_seconds_to_frames :: Double -> Int64
c_seconds_to_frames = k_seconds_to_frames c_sample_rate

-- | 'recip' of 'w_sample_rate'
w_sample_dur :: Df Double
w_sample_dur = recip w_sample_rate

-- | 'twopi' '/' 'w_sample_rate'
w_radians_per_sample :: Df Double
w_radians_per_sample = twopi / w_sample_rate

{-
-- | 'w_sample_rate' '/' 'w_block_nframes'
w_block_rate :: Df Double
w_block_rate = w_sample_rate / df_int32_to_float w_block_nframes
-}

-- * Phasor

-- | If 'q >= p' then 'q - p' else 'q'.
clipr :: K_Num a => Df a -> Df a -> Df a
clipr p q = select2 (q `df_gte` p) (q - p) q

-- | If 'q >= p' then 'q - p' else if 'q < 0' then 'q + p' else 'q'.
cliprz :: K_Num a => Df a -> Df a -> Df a
cliprz p q = select2 (q `df_gte` p) (q - p) (select2 (q `df_lt` 0) (q + p) q)

-- | 'clip2' variant (clip2 requires Ord).
df_clip2 :: K_Num a => Df a -> Df a -> Df a
df_clip2 p q =
  let nq = negate q
  in df_min q (df_max p nq)

{-
-- | Generic iir1, recursion function is given as argument.
iir1_g :: (t -> (a -> (b,b)) -> u) -> t -> (v -> a -> b) -> v -> u
iir1_g rec_f y0 f i = rec_f y0 (split . f i)
-}

iir1P :: K a => a -> (Df a -> Df a -> Df a) -> Df a -> Df a
iir1P y0 f i = knotP y0 (split . f i)

-- | Single place infinite impulse response filter with indicated initial value.
iir1 :: K a => Df a -> (Df a -> Df a -> Df a) -> Df a -> Df a
iir1 y0 f i = knot y0 (split . f i)

-- | phasor, /r/ = right hand edge, /ip/ = initial phase, /x/ = increment
phasor :: K_Num a => Df a -> Df a -> Df a -> Df a
phasor = lift_mce3 (\r ip x -> unitdelay ip (iir1 ip (\z -> clipr r . (+ z)) x))

-- * Vector

vecallocF :: ([Double] -> t) -> Int64 -> t
vecallocF vec_f nf = vec_f (genericReplicate (nf + 1 + 1) 0)

vecallocId :: VecId -> Int64 -> Df (Vec Double)
vecallocId k = vecallocF (df_vec_id k)

vecallocM :: Int64 -> IO (Df (Vec Double))
vecallocM = liftVec1 vecallocId

vecalloc :: Int64 -> Df (Vec Double)
vecalloc = Unsafe.liftUnsafe1 vecallocM

-- | Linear interpolating variant of 'vecread'.
tblLerp :: Df (Tbl Double) -> Df Double -> Df Double
tblLerp a i =
  let i_f = df_floor i
      i_c = df_ceil i
      z = i - i_f
      p = vecread a (df_lrint i_f)
      q = vecread a (df_lrint i_c)
  in (p * (1.0 - z)) + (q * z)

-- | Vector delay with /phasor/ argument for write index.
vecdelayPh :: Df (Vec Double) -> Df Double -> Df Int64 -> Df Int64 -> Df Double
vecdelayPh a s n wi =
  let ri = clipr n (wi + 1)
  in Mrg (vecread a ri) [vecwrite a wi s]

-- | Vector delay.  a = array, s = signal, n = number of frames.
vecdelay :: Df (Vec Double) -> Df Double -> Df Int64 -> Df Double
vecdelay a s n = vecdelayPh a s n (phasor n 0 1)

-- | Sc3 Ugen, max-delay time is in frames.
delayn :: Df Double -> Double -> Df Double -> Df Double
delayn s mx dt = vecdelay (vecalloc (c_seconds_to_frames mx)) s (df_lrint (dt * w_sample_rate))

-- * Table

{- | Add guard point.

>>> listAddGuard [1,2,3]
[1,2,3,1]
-}
listAddGuard :: [a] -> [a]
listAddGuard t =
  case t of
    [] -> []
    i : _ -> t ++ [i]

{- | Generate guarded sin table.

>>> map (round . (* 100)) (listGenSin 12)
[0,50,87,100,87,50,0,-50,-87,-100,-87,-50,0]
-}
listGenSin :: Floating n => Int -> [n]
listGenSin n =
  let f = sin . (* 2) . (* pi) . (/ fromIntegral n) . fromIntegral
  in listAddGuard (map f [0 .. n - 1])

-- | Table gen function (sin).
tblGenSinId :: TblId -> Int -> Df (Tbl Double)
tblGenSinId k = df_vec_id k . listGenSin

-- | Sin table of 8192 places.
tblSin8192 :: Df (Tbl Double)
tblSin8192 = tblGenSinId vecIdSin8192 8192

tblGenSinM :: Int -> IO (Df (Tbl Double))
tblGenSinM = liftVec1 tblGenSinId

tblGenSin :: Int -> Df (Vec Double)
tblGenSin = Unsafe.liftUnsafe1 tblGenSinM

-- * Table oscillator

{- | 'phasor' for table of /tblSize/ places. /initPhase/ is in (0,1).

> draw (phasor 64 (0::Double) (Math.cps_to_incr w_sample_rate 64.0 330.0))
> draw (tblPhasor 64 0 330)
-}
tblPhasor :: Int -> Double -> Df Double -> Df Double
tblPhasor tblSize initPhase freq =
  let z_r = fromIntegral tblSize
      z_c = K z_r
      ip_c = K (initPhase * z_r)
  in phasor z_c ip_c (Math.cps_to_incr w_sample_rate z_c freq)

-- | Linear interpolating table lookup oscillator.  phase is in (-2ip,2pi)
tblosc :: Df (Tbl Double) -> Df Double -> Df Double -> Df Double
tblosc a f phase =
  let zi = fromMaybe 0 (df_tbl_size a)
      zk = K (fromIntegral zi)
      ph = tblPhasor zi 0 f + ((phase * (1 / (2 * pi))) * zk)
  in tblLerp a (cliprz zk ph)

-- * Vector

-- | Infinite sequence of values from vector.
vecseq :: Df (Vec Double) -> Df Double
vecseq a =
  let z = fromMaybe 0 (df_vec_size a)
      p = phasor (fromIntegral z) 0 1
  in vecread a p

vecshuffleId :: Df Rng64 -> Df (Vec Double) -> Df Double
vecshuffleId r a =
  let z = fromMaybe 0 (df_vec_size a)
      p = irandId r 0 (fromIntegral z)
  in vecread a p

vecshuffleM :: Df (Vec Double) -> IO (Df Double)
vecshuffleM = liftRng1 vecshuffleId

vecshuffle :: Df (Vec Double) -> Df Double
vecshuffle = Unsafe.liftUnsafe1 vecshuffleM

-- * Filter constructors.

unitdelayP :: K a => a -> Df a -> Df a
unitdelayP y0 s = knotP y0 (\i -> (i, s))

-- | Single sample delay with indicated initial value.
unitdelay :: K a => Df a -> Df a -> Df a
unitdelay y0 s = knot y0 (\i -> (i, s))

-- | Signal that is initially 'True' then always 'False'.
unittrigger :: Df Bool
unittrigger = unitdelayP True (K False)

{- | Two place infinite impulse response filter.  Inputs are: /f/=
function @(\x0 y1 y2 -> y0)@, /i/ = input signal.
-}
iir2 :: (Ternary_Op (Df Double)) -> Df Double -> Df Double
iir2 f i =
  knotP
    0
    ( split
        . ( \y1 ->
              let y2 = unitdelayP 0 y1
              in f i y1 y2
          )
    )

iir2_ff_fb :: K_Num a => (Df a -> Df a -> Df a -> (Df a, Df a)) -> Df a -> Df a
iir2_ff_fb f i =
  knotP
    0
    ( \y1 ->
        let y2 = unitdelayP 0 y1
        in f i y1 y2
    )

-- | Single place finite impulse response filter.
fir1 :: K a => a -> (Df a -> Df a -> Df b) -> Df a -> Df b
fir1 z0 f i = f i (unitdelayP z0 i)

-- | Two place finite impulse response filter.
fir2 :: (Ternary_Op (Df Double)) -> Df Double -> Df Double
fir2 f i = let x1 = delay1 i in f i x1 (delay1 x1)

-- | Ordinary biquad filter section.
biquad :: (Quinary_Op (Df Double)) -> Df Double -> Df Double
biquad f i =
  knotP
    0
    ( split
        . ( \y1 ->
              let x1 = delay1 i
                  x2 = delay1 x1
                  y2 = delay1 y1
              in f i x1 x2 y1 y2
          )
    )

-- * Counter

-- | Counter from indicated initial value by indicated step.
counter :: K_Num a => a -> Df a -> Df a
counter y0 n = unitdelayP y0 (iir1P y0 (+) n)

-- | 'counter' that resets to the initial phase at trigger.
counter_reset :: K_Num a => a -> Df a -> Df Bool -> Df a
counter_reset y0 n tr =
  let f lhs rhs = select2 tr (K y0) (lhs + rhs)
  in iir1P y0 f n

-- | Counter from 0 to 1 over duration (in seconds).  Holds end value.
unitLine :: Df Double -> Df Double
unitLine d = let c = counter 0 (w_sample_dur / d) in select2 (c `df_gt` 1) 1 c

-- | 'linlin' of 'unitLine'.
line :: Df Double -> Df Double -> Df Double -> Df Double
line s e d = linlin (unitLine d) 0 1 s e

-- | 'linexp' of 'unitLine'.
xline :: Df Double -> Df Double -> Df Double -> Df Double
xline s e d = linexp (unitLine d) 0 1 s e

-- | Sc3 Ugen (ie. break a continuous signal into line segments, not linear lag)
ramp :: Df Double -> Df Double -> Df Double
ramp s d =
  let im = impulse (1 / d) 0
      tr = trigger im
      s_l = latch s tr
      d_l = latch d tr
      n = latch ((s_l - delay1 s_l) / (d_l * w_sample_rate)) tr
  in counter 0 n

-- * Comb

combnP :: Df Double -> Df Double -> Df Double -> Df Double -> Df Double
combnP s mx dlt dct =
  let a = vecalloc (c_seconds_to_frames (df_k_err mx))
      n = df_lrint (dlt * w_sample_rate)
      fb = calc_fb_from_dt dlt dct
      c i =
        let x = vecdelay a i n
        in (x, s + (fb * x))
  in knotP 0 c

-- | Non-interpolating comb filter. Max delay time is in frames.
combn :: Df Double -> Df Double -> Df Double -> Df Double -> Df Double
combn = lift_mce4 combnP

-- * Allpass

allpassnP :: Df Double -> Df Double -> Df Double -> Df Double -> Df Double
allpassnP s mx dlt dct =
  let a = vecalloc (c_seconds_to_frames (df_k_err mx))
      n = df_lrint (dlt * w_sample_rate)
      fb = calc_fb_from_dt dlt dct
      c i =
        let t = vecdelay a i n
            u = s + (fb * t)
            o = t - (fb * u)
        in (o, u)
  in knotP 0 c

-- | Non-interpolating allpass filter.
allpassn :: Df Double -> Df Double -> Df Double -> Df Double -> Df Double
allpassn = lift_mce4 allpassnP

-- * Constant

{- | Alias for 0.

> audition_rju [] (out1 silent)
-}
silent :: Df Double
silent = 0

-- * Noise

-- | 'K' of 'Rng' of 'fromEnum'.
rngId :: Uid.ID z => z -> Df Rng64
rngId = K . Rng64 . fromIntegral . Uid.resolveID

rngId_i :: Int64 -> Df Rng64
rngId_i = K . Rng64

-- | 'mce2' of 'rng'.
rngId2 :: Uid.ID z => z -> z -> Df Rng64
rngId2 p q = mce2 (rngId p) (rngId q)

{- | 'Int64' linear congruential generator, hence signed modulo of @2^64@.
  Note that the state and all internal math is 64bit.

See <http://en.wikipedia.org/wiki/Linear_congruential_generator> for possible parameters.
-}
lcg_i64 :: Int64 -> Int64 -> Int64 -> Df Int64
lcg_i64 a c x0 =
  let tilde f g = knotP x0 (\i -> let r = f i in (r, g r))
  in ((K c) +) `tilde` (* (K a))

-- | 'lcg_i64' 6364136223846793005 1442695040888963407, so in (minBound,maxBound).
lcg_i64_knuth :: Df Rng64 -> Df Int64
lcg_i64_knuth df =
  case df of
    K (Rng64 k) -> lcg_i64 6364136223846793005 1442695040888963407 k
    _ -> error "lcg_i64_knuth: non-constant seed?"

-- | 'abs' of 'lcg_i64_knuth', so in (0,maxBound).
rand_i64 :: Df Rng64 -> Df Int64
rand_i64 = abs . lcg_i64_knuth

{- | 'rand_i64' in (l,r), non-inclusive of r.

>>> maxBound == (9223372036854775807::Int64)
True
-}
irandId :: Df Rng64 -> Df Int64 -> Df Int64 -> Df Int64
irandId k l r = (rand_i64 k `df_div` (9223372036854775807 `df_div` (r - l))) + l

irandM :: Df Int64 -> Df Int64 -> IO (Df Int64)
irandM = liftRng2 irandId

irand :: Df Int64 -> Df Int64 -> Df Int64
irand = Unsafe.liftUnsafe2 irandM

-- | 'i64_to_normal_f64' of 'rand_i64', so in (0,1).
rand_f64 :: Df Rng64 -> Df Double
rand_f64 = i64_to_normal_f64 . rand_i64

whitenoiseLcgId :: Df Rng64 -> Df Double
whitenoiseLcgId = i64_to_normal_f64 . lcg_i64_knuth

whitenoiseLcgM :: IO (Df Double)
whitenoiseLcgM = liftRng0 whitenoiseLcgId

-- | White noise (-1,1).  Generates noise whose spectrum has equal power at all frequencies.
whitenoiseLcg :: () -> Df Double
whitenoiseLcg = Unsafe.liftUnsafe0 whitenoiseLcgM

whitenoise :: () -> Df Double
whitenoise = whitenoiseLcg

trandIdP :: Df Rng64 -> Df Double -> Df Double -> Df Bool -> Df Double
trandIdP z l r t = demand (K 0) t (linlin (whitenoiseLcgId z) (-1) 1 l r)

trandId :: Df Rng64 -> Df Double -> Df Double -> Df Bool -> Df Double
trandId = lift_mce4 trandIdP

trandM :: Df Double -> Df Double -> Df Bool -> IO (Df Double)
trandM = liftRng3 trandId

-- | Sc3 Ugen.
trand :: Df Double -> Df Double -> Df Bool -> Df Double
trand = Unsafe.liftUnsafe3 trandM

{-

This form generates shared demand graph errors:

trandP :: Df Double -> Df Double -> Df Bool -> Df Double
trandP l r t = demand (K 0) t (linlin (whitenoise ()) (-1) 1 l r)

-- | Sc3 Ugen.
trand :: Df Double -> Df Double -> Df Bool -> Df Double
trand = lift_mce3 trandP
-}

lfnoise1Id :: Df Rng64 -> Df Double -> Df Double
lfnoise1Id k d = ramp (whitenoiseLcgId k) (1 / d)

lfnoise1M :: Df Double -> IO (Df Double)
lfnoise1M = liftRng1 lfnoise1Id

-- | Sc3 Ugen.
lfnoise1 :: Df Double -> Df Double
lfnoise1 = Unsafe.liftUnsafe1 lfnoise1M

-- | 'iir1' brown noise function.
brown_noise_f :: Binary_Op (Df Double)
brown_noise_f x y1 =
  let z = x + y1
      r = select2 (z `df_lt` (-1.0)) ((-2.0) - z) z
  in select2 (z `df_gt` 1.0) (2.0 - z) r

{- | Brown noise (-1,1).  Generates noise whose spectrum falls off in
power by 6 dB per octave.
-}
brown_noise_id :: Df Rng64 -> Df Double
brown_noise_id k =
  let w = whitenoiseLcgId k
      w8 = w / 8.0
  in iir1P 0 brown_noise_f w8

brown_noise_m :: IO (Df Double)
brown_noise_m = liftRng0 brown_noise_id

brownnoise :: () -> Df Double
brownnoise = Unsafe.liftUnsafe0 brown_noise_m

{- | Approximation to a -10dB/decade filter, accurate to within +/-0.05dB above 9.2Hz
     paul.kellett@xxxx.maxim.abel.co.uk, October 1999
     <https://www.firstpr.com.au/dsp/pink-noise/>

     See also: <https://github.com/Stenzel/newshadeofpink>
-}
pink_noise_flt_pk3 :: Df Double -> Df Double
pink_noise_flt_pk3 w =
  let mk f = knotP 0 (split . f)
      b0 = mk (\x -> 0.99886 * x + w * 0.0555179)
      b1 = mk (\x -> 0.99332 * x + w * 0.0750759)
      b2 = mk (\x -> 0.96900 * x + w * 0.1538520)
      b3 = mk (\x -> 0.86650 * x + w * 0.3104856)
      b4 = mk (\x -> 0.55000 * x + w * 0.5329522)
      b5 = mk (\x -> -0.7616 * x - w * 0.0168980)
      b6 = delay1 (w * 0.115926)
  in b0 + b1 + b2 + b3 + b4 + b5 + b6 + w * 0.5362

pink_noise_pk3_id :: Df Rng64 -> Df Double
pink_noise_pk3_id z = pink_noise_flt_pk3 (whitenoiseLcgId z)

pink_noise_pk3_m :: IO (Df Double)
pink_noise_pk3_m = liftRng0 pink_noise_pk3_id

pink_noise_pk3 :: () -> Df Double
pink_noise_pk3 = Unsafe.liftUnsafe0 pink_noise_pk3_m

pinknoise :: () -> Df Double
pinknoise = pink_noise_pk3

-- | 'economy' version, accuracy of +/-0.5dB
pink_noise_flt_pke :: Df Double -> Df Double
pink_noise_flt_pke w =
  let mk f = knotP 0 (split . f)
      b0 = mk (\x -> 0.99765 * x + w * 0.0990460)
      b1 = mk (\x -> 0.96300 * x + w * 0.2965164)
      b2 = mk (\x -> 0.57000 * x + w * 1.0526913)
  in b0 + b1 + b2 + w * 0.1848

pink_noise_pke_id :: Df Rng64 -> Df Double
pink_noise_pke_id z = pink_noise_flt_pke (whitenoiseLcgId z)

pink_noise_pke_m :: IO (Df Double)
pink_noise_pke_m = liftRng0 pink_noise_pke_id

pink_noise_pke :: () -> Df Double
pink_noise_pke = Unsafe.liftUnsafe0 pink_noise_pke_m

-- | <http://doc.sccode.org/Classes/Dust.html>
dustId :: Df Rng64 -> Df Double -> Df Double
dustId k density =
  let threshold = density * w_sample_dur
      scale = select2 (threshold `df_gt` 0.0) (1.0 / threshold) 0.0
      z = rand_f64 k
  in select2 (z `df_lt` threshold) (z * scale) 0.0

dustM :: Df Double -> IO (Df Double)
dustM = liftRng1 dustId

dust :: Df Double -> Df Double
dust = Unsafe.liftUnsafe1 dustM

{-
-- | <http://doc.sccode.org/Classes/Dust.html>
dust :: Df Double -> Df Double
dust density =
    let threshold = density * w_sample_dur
        scale = select2 (threshold `df_gt` 0.0) (1.0 / threshold) 0.0
        z = rand 0 1
    in select2 (z `df_lt` threshold) (z * scale) 0.0
-}

-- | <http://doc.sccode.org/Classes/Rand.html>
randId :: Df Rng64 -> Df Double -> Df Double -> Df Double
randId k =
  let n = rand_f64 k
      z = unitdelayP True (K False)
  in linlin (latch n z) 0 1

rand_m :: Df Double -> Df Double -> IO (Df Double)
rand_m = liftRng2 randId

rand :: Df Double -> Df Double -> Df Double
rand = Unsafe.liftUnsafe2 rand_m

-- * Osc

-- | Sine oscillator.  Inputs are: /f/ = frequency (in cps), /ip/ = initial phase.
sin_osc_ip :: Df Double -> Double -> Df Double
sin_osc_ip f ip =
  let p = phasor (K twopi) (K ip) (Math.cps_to_incr w_sample_rate twopi f)
  in sin p

-- | Sine oscillator.  Inputs are: /freq/ = frequency (in cps), /phase/ = phase.
sinosc :: Df Double -> Df Double -> Df Double
sinosc freq phase =
  let p = phasor twopi 0 (Math.cps_to_incr w_sample_rate twopi freq)
  in sin (p + phase)

{- | Impulse oscillator (non band limited).
Outputs non band limited single sample unit-impulses.
Inputs are: /f/ = frequency (in hertz), /ip/ = phase offset (0..1)
-}
impulse :: Df Double -> Double -> Df Double
impulse f ip =
  let i = Math.cps_to_incr w_sample_rate 1 f
      p = phasor (K 1) (K ip) i
      s = unitdelayP (if ip > 0 then 0 else 1) p `df_gt` p
  in select2 s 1 0

-- | 'ispositive' of 'impulse'
impulse_bool :: Df Double -> Double -> Df Bool
impulse_bool f = ispositive . impulse f

{- | Single oscillator feedback FM, phase is (0,two-pi) so feebback
coefficients are approx. (0,1.5).
-}
fbfm1 :: Df Double -> Df Double -> Df Double
fbfm1 cf fb =
  knotP
    0
    ( \y0 ->
        let ph = phasor twopi 0 (Math.cps_to_incr w_sample_rate twopi cf)
            r = sin (ph + (fb * y0))
        in (r, r)
    )

-- * LF Osc.

{- | Non-band limited sawtooth oscillator.  Output ranges from -1 to +1.
Inputs are: /f/ = frequency (in hertz), /ip/ = initial phase (0,2).
-}
lfsaw :: Df Double -> Df Double -> Df Double
lfsaw freq i_phase =
  let p = phasor (K 2) i_phase (Math.cps_to_incr w_sample_rate 2.0 freq)
  in p - 1.0

{- | Non-band-limited pulse oscillator. Outputs a high value of one
and a low value of zero. Inputs are: /f/ = frequency (in hertz),
/ip/ = initial phase (0,1), /w/ = pulse width duty cycle (0,1).
-}
lfpulse :: Df Double -> Df Double -> Df Double -> Df Double
lfpulse f ip w =
  let p = phasor (K 1) ip (Math.cps_to_incr w_sample_rate 1.0 f)
  in select2 (p `df_gte` w) 0.0 1.0

-- * Filters

-- | Two zero fixed midpass filter.
bpz2 :: Df Double -> Df Double
bpz2 = fir2 (\x _ x2 -> (x - x2) * 0.5)

-- | Two zero fixed midcut filter.
brz2 :: Df Double -> Df Double
brz2 = fir2 (\x _ x2 -> (x + x2) * 0.5)

-- | Two point difference filter
hpz1 :: Df Double -> Df Double
hpz1 = fir1 0 (\x x1 -> 0.5 * (x - x1))

-- | Two zero fixed highpass filter
hpz2 :: Df Double -> Df Double
hpz2 = fir2 (\x x1 x2 -> 0.25 * (x - (2 * x1) + x2))

-- | Two point average filter
lpz1 :: Df Double -> Df Double
lpz1 = fir1 0 (\x x1 -> (x + x1) * 0.5)

-- | Two zero fixed lowpass filter
lpz2 :: Df Double -> Df Double
lpz2 = fir2 (\x x1 x2 -> (x + (2.0 * x1) + x2) * 0.25)

-- | Given /cf/ construct 'iir1' one-pole function.
one_pole_f :: Fractional a => a -> Binary_Op a
one_pole_f cf x y1 = ((1.0 - abs cf) * x) + (cf * y1)

-- | One pole filter.
onepole :: Df Double -> Df Double -> Df Double
onepole i cf = iir1P 0 (one_pole_f cf) i

-- | Given /cf/ construct 'fir1' one-zero function.
one_zero_f :: Fractional a => a -> Binary_Op a
one_zero_f cf x x1 = ((1.0 - abs cf) * x) + (cf * x1)

-- | One zero filter.
onezero :: Df Double -> Df Double -> Df Double
onezero i cf = fir1 0 (one_zero_f cf) i

-- | Given coefficients construct 'biquad' 'sos' function.
sos_f :: Num a => a -> a -> a -> a -> a -> Quinary_Op a
sos_f a0 a1 a2 b1 b2 x x1 x2 y1 y2 = a0 * x + a1 * x1 + a2 * x2 - b1 * y1 - b2 * y2

-- | Second order filter section.
sos :: Df Double -> Df Double -> Df Double -> Df Double -> Df Double -> Df Double -> Df Double
sos i a0 a1 a2 b1 b2 = biquad (sos_f a0 a1 a2 b1 b2) i

rlpfP :: Df Double -> Df Double -> Df Double -> Df Double
rlpfP i f r = iir2 (Hs.rlpf_f df_max (w_radians_per_sample, f, r)) i

{- | Resonant low pass filter, non-MCE.
     Inputs are: /i/ = input signal, /f/ = frequency (hertz), /rq/ = reciprocal of Q (resonance).
-}
rlpf :: Df Double -> Df Double -> Df Double -> Df Double
rlpf = lift_mce3 rlpfP

resonzP :: Df Double -> Df Double -> Df Double -> Df Double
resonzP i f r = iir2_ff_fb (Hs.resonz_f (w_radians_per_sample, f, r)) i

-- | Resonant filter, non-MCE. Inputs are as for rlpf.
resonz :: Df Double -> Df Double -> Df Double -> Df Double
resonz = lift_mce3 resonzP

-- | High pass filter.
hpf :: Df Double -> Df Double -> Df Double
hpf i f =
  let sr = w_sample_rate
      (a0, a1, a2, b1, b2) = Filter.bw_lpf_or_hpf_coef True sr f
  in sos i a0 a1 a2 b1 b2

-- | Low pass filter.
lpf :: Df Double -> Df Double -> Df Double
lpf i f =
  let sr = w_sample_rate
      (a0, a1, a2, b1, b2) = Filter.bw_lpf_or_hpf_coef False sr f
  in sos i a0 a1 a2 b1 b2

-- * Triggers

-- | `df_gt` @0@.
ispositive :: K_Num a => Df a -> Df Bool
ispositive x = x `df_gt` 0

-- | 'df_not' of 'positive'.
isnonpositive :: K_Num a => Df a -> Df Bool
isnonpositive = df_not . ispositive

-- | 'fir1' /trigger/ function.
trigger_f :: K_Num a => Df a -> Df a -> Df Bool
trigger_f x x1 = ispositive x `df_and` isnonpositive x1

-- | True on non-positive to positive transition.
trigger :: K_Num a => Df a -> Df Bool
trigger = fir1 0 trigger_f

-- | Count 'True' values at input.
count_true :: K_Num a => Df Bool -> Df a
count_true s = knotP 0 (\y1 -> split (select2 s (y1 + 1) y1))

pulsedividerBoolP :: Df Bool -> Df Int64 -> Df Int64 -> Df Bool
pulsedividerBoolP tr n st =
  let c = count_true tr + st
  in tr `df_and` ((c `df_mod` n) `df_eq` 0)

-- | Pulse divider at 'Bool'
pulsedividerBool :: Df Bool -> Df Int64 -> Df Int64 -> Df Bool
pulsedividerBool = lift_mce3 pulsedividerBoolP

pulsedividerP :: K_Num a => Df a -> Df Int64 -> Df Int64 -> Df a
pulsedividerP tr n =
  let f x = select2 x 1 0
  in f . pulsedividerBoolP (trigger tr) n

-- | Sc3 PulseDivider.
pulsedivider :: K_Num a => Df a -> Df Int64 -> Df Int64 -> Df a
pulsedivider = lift_mce3 pulsedividerP

{- | Sample and hold.
Holds input signal value when triggered.
Inputs are: /i/ = input signal, /t/ = trigger.
-}
latch :: K_Num a => Df a -> Df Bool -> Df a
latch i t = iir1P 0 (select2 t) i

-- * Decays

-- | Given /dt/ construct 'iir1' 'decay' function.
decay_f :: Df Double -> Binary_Op (Df Double)
decay_f = Hs.decay_f w_sample_rate

decayP :: Df Double -> Df Double -> Df Double
decayP i dt = iir1P 0 (decay_f dt) i

{- | Exponential decay. Inputs are: /i/ = input signal, /t/ = decay
time.  This is essentially the same as Integrator except that
instead of supplying the coefficient directly, it is caculated from
a 60 dB decay time. This is the time required for the integrator to
lose 99.9 % of its value or -60dB. This is useful for exponential
decaying envelopes triggered by impulses.
-}
decay :: Df Double -> Df Double -> Df Double
decay = lift_mce2 decayP

-- | Exponential decay (equivalent to @decay dcy - decay atk@).
decay2 :: Df Double -> Df Double -> Df Double -> Df Double
decay2 i atk dcy = decay i dcy - decay i atk

-- * Delays

-- | Single sample delay.
delay1 :: Df Double -> Df Double
delay1 = unitdelayP 0

-- | Two sample delay.
delay2 :: Df Double -> Df Double
delay2 = fir2 (\_ _ x -> x)

-- * Lags

lag_frames :: Df Double -> Df Double -> Df Double
lag_frames i t = iir1P 0 (Hs.lag_f_frames t) i

-- | Simple averaging filter.  Inputs are: /i/ = input signal, /t/ = lag time (seconds).
lag :: Df Double -> Df Double -> Df Double
lag i t = iir1P 0 (Hs.lag_f w_sample_rate t) i

-- | Nested lag filter.
lag2 :: Df Double -> Df Double -> Df Double
lag2 i t = lag (lag i t) t

-- | Twice nested lag filter.
lag3 :: Df Double -> Df Double -> Df Double
lag3 i t = lag (lag (lag i t) t) t

-- * Sequencing

-- | 'vecread' of 'df_vec_id', indexing function receives table size.
a_tbl_read_n :: VecId -> [Double] -> (Int64 -> Df Int64) -> Df Double
a_tbl_read_n z l c_f =
  let a = df_vec_id z l
      n = genericLength l
  in vecread a (c_f n)

-- | Demand sequencer, index increments when /tr/ is True.
sequ_id :: VecId -> [Double] -> Df Bool -> Df Double
sequ_id z l tr = demand (K 0) tr (a_tbl_read_n z l (\n -> phasor (K n) 0 1))

sequ_m :: [Double] -> Df Bool -> IO (Df Double)
sequ_m = liftVec2 sequ_id

sequ :: [Double] -> Df Bool -> Df Double
sequ = Unsafe.liftUnsafe2 sequ_m

-- | Audio rate variant of 'sequ'.
sequ_ar_id :: VecId -> [Double] -> Df Bool -> Df Double
sequ_ar_id z l tr = a_tbl_read_n z l (\n -> count_true tr `df_mod` K n)

sequrandId :: Df Rng64 -> VecId -> [Double] -> Df Bool -> Df Double
sequrandId k z l tr = demand (K 0) tr (a_tbl_read_n z l (\n -> irandId k 0 (K n)))

sequrandM :: [Double] -> Df Bool -> IO (Df Double)
sequrandM l tr = do
  k <- generateRng64
  z <- nextVecId
  return (sequrandId (K k) z l tr)

-- | Random sequencer.
sequrand :: [Double] -> Df Bool -> Df Double
sequrand = Unsafe.liftUnsafe2 sequrandM

-- * Control Rate

{- | Rewrite primitive df_sample_rate as df_sample_rate / dv.

>>> df_kr_rewrite 64 (P0 "df_sample_rate" Double_t)
P2 "df_fdiv" Double_t (P0 "df_sample_rate" Double_t) (K 64.0)
-}
df_kr_rewrite :: Double -> Df t -> Df t
df_kr_rewrite dv u =
  case u of
    K _ -> u
    MemRd _ _ -> u
    MemWr z r x -> MemWr z r (df_kr_rewrite dv x)
    P0 "df_sample_rate" ty -> P2 "df_fdiv" ty u (K dv)
    P0 _ _ -> u
    P1 nm ty p1 -> P1 nm ty (df_kr_rewrite dv p1)
    P2 nm ty p1 p2 -> P2 nm ty (df_kr_rewrite dv p1) (df_kr_rewrite dv p2)
    P3 nm ty p1 p2 p3 -> P3 nm ty (df_kr_rewrite dv p1) (df_kr_rewrite dv p2) (df_kr_rewrite dv p3)
    Dmd _ _ _ -> error "df_kr_rewrite: dmd?"
    Ref _ _ -> error "df_kr_rewrite: ref?"
    Mce m -> Mce (map (df_kr_rewrite dv) m)
    Mrg lhs rhs -> Mrg (df_kr_rewrite dv lhs) (map (df_kr_rewrite dv) rhs)

{- | kr_frm transforms the graph /gr/ to run at control-rate, specified
as an integer division of the audio sample-rate, ie. the number of
frames per control block.
-}
kr_frm :: (K a, Num a) => Int64 -> Df a -> Df a
kr_frm dv gr =
  let tr = trigger (phasor (K dv) 0 1)
  in demand (K 0) tr (df_kr_rewrite (fromIntegral dv) gr)

-- | kr_frm 48.  Ought to be k_sample_rate / 1000, ie. 1ms
kr :: (K a, Num a) => Df a -> Df a
kr = kr_frm 48

{- | If p then lhs else rhs.  'select2' p of 'demand' p of lhs and 'demand' not p of rhs.
  This variant allows a default value, though it is unused.
-}
if_then_else_def :: K a => a -> Df Bool -> Df a -> Df a -> Df a
if_then_else_def z p lhs rhs = select2 p (demand (K z) p lhs) (demand (K z) (df_not p) rhs)

-- | 'if_then_else_def' of zero, hence 'Num' contraint.
if_then_else :: (K a, Num a) => Df Bool -> Df a -> Df a -> Df a
if_then_else = if_then_else_def 0

-- | Sc3 Ugen
select :: (Num t, K t) => Df Int64 -> [Df t] -> Df t
select z =
  let f l =
        case l of
          [] -> K 0
          (ix, el) : l' -> select2 (df_eq z (K ix)) el (f l')
  in f . zip [0 ..]

-- | Infinite sequence of values from list, c.f. a_seq.
l_seq :: (Num t, K t) => [Df t] -> Df t
l_seq l = select (phasor (genericLength l) 0 1) l

-- * Fill

mixfill :: K_Num t => Int -> (Int -> Df t) -> Df t
mixfill k f = sum (map f [0 .. k - 1])

mcefill :: K_Num t => Int -> (Int -> Df t) -> Df t
mcefill k f = Mce (map f [0 .. k - 1])
