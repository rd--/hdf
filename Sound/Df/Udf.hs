-- | Udf = Un-typed data-flow.
module Sound.Df.Udf where

import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import qualified Data.Graph.Inductive as Graph {- fgl -}

import Sound.Osc.Core {- hosc -}

import qualified Sound.Df.Ll.Audition as Ll.Audition
import qualified Sound.Df.Ll.Cgen as Ll.Cgen
import Sound.Df.Ll.K

-- | Primitive name
type P_Name = String

{- | Un-typed data-flow node.

Demand sub-graphs may have multiple outputs, but Udf has no Mce type.
Ref acts as a Proxy to Dmd.
The current implementation is not careful.
MemRd and MemWr share a MemVar, however MemRd copies the MemVar into a StdVar.
This indirection is required to allow recursive MemWr (ie. delay1 of delay1).

The multiple levels (Gadt.Df -> Udf -> Ll.CGEN) make the system difficult to understand.

K = constant (including arrays)
MemRd/MemRd = recursion,
P = primitive,
DMD = demand,
Ref = reference (to demand output)
Mrg = multiple root graph.
-}
data Udf
  = Udf_K {udf_k :: K_Ty}
  | Udf_MemRd {udf_memrd_memid :: MemId, udf_memrd_default :: K_Ty}
  | Udf_MemWr {udf_memwr_memid :: MemId, udf_memwr_default :: K_Ty, udf_memwr_input :: Udf}
  | Udf_P {udf_p_name :: P_Name, udf_p_type :: Df_Type, udf_p_inputs :: [Udf]}
  | Udf_Dmd {udf_dmd_ty :: K_Ty, udf_dmd_tr :: Udf, udf_dmd_gr :: [Udf]}
  | Udf_Ref {udf_ref_src :: Udf, udf_ref_ix :: Int}
  | Udf_Mrg Udf [Udf]
  deriving (Eq, Show)

udf_is_memrd :: Udf -> Bool
udf_is_memrd udf = case udf of Udf_MemRd {} -> True; _ -> False

udf_is_memwr :: Udf -> Bool
udf_is_memwr udf = case udf of Udf_MemWr {} -> True; _ -> False

-- | Is 'Udf' a demand node?
udf_is_demand :: Udf -> Bool
udf_is_demand u = case u of Udf_Dmd {} -> True; _ -> False

-- | Maybe variant of 'udf_k'.
udf_k_m :: Udf -> Maybe K_Ty
udf_k_m n = case n of Udf_K x -> Just x; _ -> Nothing

-- | Is 'Udf' a constant?
udf_is_constant :: Udf -> Bool
udf_is_constant = isJust . udf_k_m

-- | Maybe variant of 'udf_p_name'.
udf_p_name_m :: Udf -> Maybe P_Name
udf_p_name_m n = case n of Udf_P nm _ _ -> Just nm; _ -> Nothing

udf_p_inputs_m :: Udf -> Maybe [Udf]
udf_p_inputs_m n = case n of Udf_P _ _ i -> Just i; _ -> Nothing

-- | Is Udf a block-init-rate parameter?
udf_is_block_init_rate :: Udf -> Bool
udf_is_block_init_rate u =
  case udf_p_name_m u of
    Just nm ->
      nm `elem` ["df_sample_rate", "df_block_nframes"]
        || (let i = udf_p_inputs u in not (null i) && (all udf_is_block_init_rate i))
    _ -> False

-- | Is Udf a primitive with no inputs?
udf_is_p_src :: Udf -> Bool
udf_is_p_src u = udf_p_inputs_m u == Just []

-- | Concise pretty printer for 'Udf'.
udf_concise :: Udf -> String
udf_concise n =
  case n of
    Udf_K k -> k_concise k
    Udf_MemRd z x -> printf "MemRd:%d:%s" z (k_concise x)
    Udf_MemWr z _ _ -> printf "MemWr:%d" z
    Udf_P nm ty _ -> printf "%s:%s" nm (show ty)
    Udf_Dmd _ l r -> printf "dmd(%s,[%s])" (udf_concise l) (intercalate "," (map udf_concise r))
    Udf_Ref src ix -> printf "ref(%s,%d)" (udf_concise src) ix
    Udf_Mrg l r -> printf "mrg(%s,[%s])" (udf_concise l) (intercalate "," (map udf_concise r))

{- | List all antecedents of /n/ in left biased order, includes /n/.
  Result may contain duplicates.
-}
udf_elem :: Udf -> [Udf]
udf_elem n =
  case n of
    Udf_K _ -> [n]
    Udf_P _ _ i -> n : concatMap udf_elem i
    Udf_MemRd _ _ -> [n]
    Udf_MemWr _ _ src -> n : udf_elem src
    Udf_Dmd _ l r -> n : (udf_elem l ++ concatMap udf_elem r)
    Udf_Ref src _ -> n : udf_elem src
    Udf_Mrg l r -> n : (udf_elem l ++ concatMap udf_elem r)

-- | 'nub' of 'udf_elem'.
udf_elem_uniq :: Udf -> [Udf]
udf_elem_uniq = nub . udf_elem

assert_all_equal :: Eq t => String -> [t] -> t
assert_all_equal msg x = if length x > 0 && length (nub x) == 1 then head x else error msg

-- | Output type of 'Udf'.  See into Ref and Mrg.
udf_type_of :: Udf -> Df_Type
udf_type_of df =
  case df of
    Udf_K k -> k_df_type_of k
    Udf_P _ t _ -> t
    Udf_MemRd _ x -> k_df_type_of x
    Udf_MemWr _ _ _ -> Nil_t
    Udf_Dmd _ _ n -> assert_all_equal "udf_type_of: dmd" (map udf_type_of n) -- ?
    Udf_Ref src _ -> udf_type_of src
    Udf_Mrg n _ -> udf_type_of n

-- * Graph

-- | Index for input port.  This is only used for graph drawing.
type PortIndex = Int

-- | Node identifier (Int)
type NodeId = Int

-- | A node is a 'Udf' with associated 'NodeId'.
type Node = (NodeId, Udf)

-- | Edge from left hand side node to right hand side port.
type Edge = (NodeId, NodeId, PortIndex)

-- | A graph is a list of 'Node's and 'Edge's.
type Graph = ([Node], [Edge])

-- | A variant graph form, a kind of adjaceny list, indicating the /in/ edges for each 'Node'.
type Adj_Graph = [(Node, [Edge])]

-- | 'NodeId' of 'Node'.
node_id :: Node -> NodeId
node_id = fst

-- | 'Udf' of 'Node'.
node_udf :: Node -> Udf
node_udf = snd

-- | Lookup Udf given NodeId.
lookupUdf :: [Node] -> NodeId -> Udf
lookupUdf ns k = fromMaybe (error "lookupUdf") (lookup k ns)

-- | Lookup NodeId of Udf in list of Nodes.  Sees through Mrg.
label :: [Node] -> Udf -> NodeId
label ns u =
  case u of
    Udf_Mrg lhs _ -> label ns lhs
    _ ->
      let r = find ((== u) . node_udf) ns
      in maybe (error ("label: " ++ show u)) node_id r

-- | Lookup source NodeId of Udf in list of Nodes, see through Mrg.
source :: [Node] -> Udf -> NodeId
source ns u =
  case u of
    Udf_Mrg lhs _ -> source ns lhs
    _ -> label ns u

-- | List /incoming/ edges to Udf.
edges :: [Node] -> Udf -> [Edge]
edges ns u =
  let f i k = (source ns i, label ns u, k)
  in case u of
      Udf_P _ _ is -> zipWith f is [0 ..]
      Udf_MemRd _ _ -> [] -- MemRd reads from a MemVar which is not an edge
      Udf_MemWr _ _ src -> [(source ns src, label ns u, 0)]
      Udf_Dmd _ tr gr -> zipWith f (tr : gr) [0 ..]
      Udf_Ref src ix -> [f src ix] -- ?
      _ -> []

-- * Adj_Graph

adj_graph_to_graph :: Adj_Graph -> Graph
adj_graph_to_graph = (\(n, e) -> (n, concat e)) . unzip

graph_to_adj_graph :: Graph -> Adj_Graph
graph_to_adj_graph (n, e) =
  let f k = filter (\(_, k', _) -> k == k') e
  in map (\(k, u) -> ((k, u), f k)) n

-- | Label nodes and list incoming edges.  Multiple-root nodes are erased.
mk_adj_graph :: [Udf] -> Adj_Graph
mk_adj_graph ns =
  let l_ns = zip [1 ..] ns
      w_es (k, n) = ((k, n), edges l_ns n)
      rem_mrg ((_, n), _) =
        case n of
          Udf_Mrg _ _ -> False
          _ -> True
  in filter rem_mrg (map w_es l_ns)

-- | 'mk_adj_graph' . 'udf_elem_uniq'
udf_to_adj_graph :: Udf -> Adj_Graph
udf_to_adj_graph = mk_adj_graph . udf_elem_uniq

adj_graph_select_by_id :: [NodeId] -> Adj_Graph -> Adj_Graph
adj_graph_select_by_id k_set = filter ((`elem` k_set) . node_id . fst)

adj_graph_remove_by_id :: [NodeId] -> Adj_Graph -> Adj_Graph
adj_graph_remove_by_id k_set = filter ((`notElem` k_set) . node_id . fst)

-- * Demand

{- | Collect complete demand sub-graphs.

The nodes of demand sub-graphs are partitioned into separate sequences.
Constant and block-init-rate nodes are not considered and can be shared.
The sub-graph nodes are selected and placed after each condition node.
The topological sort of the whole graph remains valid.
-}
adj_graph_demand_subgraphs_complete :: Adj_Graph -> [(NodeId, [NodeId])]
adj_graph_demand_subgraphs_complete a =
  let n = map fst a
      a_dmd = filter (udf_is_demand . node_udf . fst) a
      p_sel u =
        not
          ( udf_is_constant u
              || udf_is_block_init_rate u
              || udf_is_p_src u
              || udf_is_memrd u
          )
      u_f = filter p_sel . concatMap udf_elem_uniq . udf_dmd_gr . snd . fst
      d_u = map u_f a_dmd
  in zip (map (fst . fst) a_dmd) (map (map (label n)) d_u)

{- | Partition non-isolate nodes at sub-graph.

Locates elements of the sub-graph that are inputs to nodes outside of the sub-graph.
-}
demand_subgraph_redact :: [Edge] -> (NodeId, [NodeId]) -> (NodeId, ([NodeId], [NodeId]))
demand_subgraph_redact e_set (k, k_set) =
  let input_to z = mapMaybe (\(l, r, _) -> if z == l then Just r else Nothing) e_set
      is_local z = (input_to z \\ (k : k_set)) == []
  in (k, partition is_local k_set)

{- | 'demand_subgraph_redact' of 'adj_graph_demand_subgraphs_complete'.

NOTE: presently requires that the sub-graph be self-contained.
Alternately connected nodes could be "redacted" to the normal node sequence.
This is not entirely straightforwards, and cannot work for some recursive structures.
Also, if the sub-graph occurence is the earliest reference to the node it must be moved.
This could be made an option (ie. allow/disallow).
-}
adj_graph_demand_subgraphs :: Adj_Graph -> [(NodeId, [NodeId])]
adj_graph_demand_subgraphs a =
  let d = adj_graph_demand_subgraphs_complete a
      e = concatMap snd a
      -- allow_r (k,(k_set,_)) = (k,k_set)
      disallow_r (k, (k_set, r_set)) =
        if null r_set
          then (k, k_set)
          else
            error
              ( show
                  ( "demand rate sub-graph references external nodes: "
                  , adj_graph_select_by_id r_set a
                  )
              )
  in map (disallow_r . demand_subgraph_redact e) d

-- | Partition Adj_Graph into audio-rate graph and a set of demand-rate graphs.
adj_graph_demand_partition :: Adj_Graph -> (Adj_Graph, [(NodeId, Adj_Graph)])
adj_graph_demand_partition a =
  let d = adj_graph_demand_subgraphs a
      a' = adj_graph_remove_by_id (concatMap snd d) a
      d' = map (\(k, k_set) -> (k, adj_graph_select_by_id k_set a)) d
  in (a', d')

-- * Make graph

{- | Generate graph (node list and edge list).
     'mk_adj_graph' of 'udf_elem_uniq'

> import Sound.Df.Gadt {\- hdf -\}
> import qualified Sound.Df.Udf as U {\- hdf -\}

> let g = out1 (iir1 (0.0::Double) (+) 1)
> :t g
> > g :: Df ()

> let c = df_lower g
> :t c
> > c :: U.Udf

> unwords $ map U.udf_concise (U.udf_elem_uniq c)
> > df_out1:() recWr df_add:Double 1.0 recRd:0.0 df_add:Double 1.0 recRd:0.0
-}
udf_to_graph :: Udf -> Graph
udf_to_graph n =
  let a = mk_adj_graph (udf_elem_uniq n)
      (ns, es) = unzip a
  in (ns, concat es)

-- * FGL Graph

-- | FGL graph with 'Udf' label.
type Gr = Graph.Gr Udf PortIndex

graph_to_gr :: Graph -> Gr
graph_to_gr = uncurry Graph.mkGraph

-- | Generate 'Gr'.
adj_graph_to_gr :: Adj_Graph -> Gr
adj_graph_to_gr = graph_to_gr . adj_graph_to_graph

-- | Topological sort of nodes (via 'udf_gr').
adj_graph_tsort :: Adj_Graph -> [Udf]
adj_graph_tsort ag =
  let gr = adj_graph_to_gr ag
  in map (fromMaybe (error "tsort") . Graph.lab gr) (Graph.topsort gr)

-- * Code Gen

{- | List of required variable declarations.
     For demand, this is the set of variable to store state between updates.
     Demand allows for multiple channel sub-graphs.
     MemRd and MemWr communicate via a shared memvar, identified by MemId.
-}
node_vars :: Node -> [Ll.Cgen.Var]
node_vars (k, df) =
  case df of
    Udf_K i -> [Ll.Cgen.k_var k Ll.Cgen.Std_Var i]
    -- MemRd declares the reader StdVar (value is copied into StdVar from shared MemVar)
    Udf_MemRd _ x -> [Ll.Cgen.k_var k Ll.Cgen.Std_Var x]
    -- MemWr declares the shared reader/writer MemVar
    Udf_MemWr z x _ -> [Ll.Cgen.k_var z (Ll.Cgen.Mem_Var z) x]
    Udf_P _ ty _ ->
      if ty == Nil_t
        then []
        else [(Ll.Cgen.Std_Var, ty, k, Nothing)]
    Udf_Dmd def _ r ->
      let ty = assert_all_equal "node_vars: dmd" (map udf_type_of r)
          mk ix = (Ll.Cgen.Dmd_Var ix, ty, k, Just (Ll.Cgen.k_to_var_fld def))
      in map mk [0 .. length r - 1]
    Udf_Ref src _ -> [(Ll.Cgen.Std_Var, udf_type_of src, k, Nothing)] -- ?
    Udf_Mrg _ _ -> error "node_vars_n: Mrg"

lookup_err :: Eq k => k -> [(k, v)] -> v
lookup_err k = fromMaybe (error "lookup?") . lookup k

map_with_index :: ((a, Int) -> b) -> [a] -> [b]
map_with_index f l = map f (zip l [0 .. length l - 1])

-- | c-call code statements, maybe be empty.
node_c_call :: [(NodeId, Adj_Graph)] -> (Node, [Edge]) -> [Ll.Cgen.C_Call]
node_c_call a_dmd ((k, n), es) =
  let fc c p q = (Just c, Just p, q, ";")
      edge_to_var (src, _, _) = (Ll.Cgen.Std_Var, src)
  in case (n, es) of
      (Udf_K _, []) -> []
      (Udf_MemRd z _, []) ->
        [fc (printf "MemRd:%d" z) "df_copy" [(Ll.Cgen.Std_Var, k), (Ll.Cgen.Mem_Var z, k)]]
      (Udf_MemWr z _ _, [e0]) ->
        [fc (printf "MemWr:%d" z) "df_copy" [(Ll.Cgen.Mem_Var z, k), edge_to_var e0]]
      (Udf_P a t _, _) ->
        let o_l =
              if t /= Nil_t
                then [(Ll.Cgen.Std_Var, k)]
                else []
        in [fc "P" a (o_l ++ map edge_to_var es)]
      (Udf_Dmd _ _ _, e0 : eN) ->
        -- The sub-graphs are run inside an if statement over the first (condition) in-edge.
        -- At the end the remaining in-edges (outputs, results) are stored to state variables.
        concat
          [ [(Nothing, Just "if", [edge_to_var e0], "{")]
          , concatMap (node_c_call a_dmd) (lookup_err k a_dmd)
          , map_with_index
              (\(e, ix) -> fc "Dmd" "df_copy" [(Ll.Cgen.Dmd_Var ix, k), e])
              (map edge_to_var eN)
          , [(Just "endif", Nothing, [], "}")]
          ]
      (Udf_Ref _ _, [(r, _, ix)]) -> [fc "Ref" "df_copy" [(Ll.Cgen.Std_Var, k), (Ll.Cgen.Dmd_Var ix, r)]] -- ?
      _ -> error ("node_c_call: " ++ show (n, es))

-- | Constant nodes.
k_nodes :: [Node] -> [(NodeId, K_Ty)]
k_nodes ns =
  let ks = filter (isJust . udf_k_m . node_udf) ns
  in map (fmap udf_k) ks

-- | Generate 'Instructions' from 'Udf'.
udf_instructions :: Udf -> Ll.Cgen.Instructions
udf_instructions u =
  let a = mk_adj_graph (adj_graph_tsort (udf_to_adj_graph u))
      (a_nrm, a_dmd) = adj_graph_demand_partition a
      (a_nrm_init, a_nrm_dsp) = partition (udf_is_block_init_rate . snd . fst) a_nrm
      ns = map fst a
      ks = k_nodes ns
      vs = concatMap node_vars ns
      cc_init = concatMap (node_c_call []) a_nrm_init
      -- MemWr nodes must run after all MemRd nodes
      -- All MemWr nodes are moved to the end
      -- The ordering of MemWr nodes is free because MemRd stores values into StdVar
      (a_nrm_dsp_memwr, a_nrm_dsp_std) = partition (udf_is_memwr . snd . fst) a_nrm_dsp
      cc_dsp = concatMap (node_c_call a_dmd) (a_nrm_dsp_std ++ a_nrm_dsp_memwr)
  in (ks, vs, (cc_init, cc_dsp))

-- | 'dl_gen' of 'udf_instructions'.
udf_dl_gen :: FilePath -> (Ll.Cgen.Host, FilePath) -> Udf -> IO ()
udf_dl_gen fn hd = Ll.Cgen.dl_gen fn hd . udf_instructions

-- * Audition

-- | Audition graph after sending initialisation messages.
audition :: [Message] -> Udf -> IO ()
audition is n = Ll.Audition.audition_rju is (udf_instructions n)

-- | Audition graph after sending initialisation messages.
audition_sc3 :: [Message] -> Udf -> IO ()
audition_sc3 is n = Ll.Audition.audition_sc3 is (udf_instructions n)

-- | Audition at @text-dl@.
audition_text :: Int -> Udf -> IO ()
audition_text nf n = Ll.Audition.audition_text nf (udf_instructions n)
