-- | Udf graph drawing.
module Sound.Df.Udf.Dot where

import Data.Maybe {- base -}
import System.Directory {- directory -}
import System.Environment {- base -}
import System.FilePath {- filepath -}
import System.Process {- process -}
import Text.Printf {- base -}

import qualified Data.Graph.Inductive as Graph {- fgl -}
import qualified Data.Graph.Inductive.Dot as Graph {- fgl-visualize -}

import qualified Sound.Df.Ll.Dot as Ll.Dot
import Sound.Df.Ll.K
import Sound.Df.Udf

-- | Make dot_rec_nil /arguments/ input.
dot_arg :: [Udf] -> [Either Int K_Ty]
dot_arg =
  let f (i, u) = case u of
        Udf_K k -> Right k
        _ -> Left i
  in map f . zip [0 ..]

dot_draw_constant_nodes :: Bool
dot_draw_constant_nodes = False

-- | Dot notation of 'Node'.
dot_node :: Node -> String
dot_node (k, u) =
  case u of
    Udf_K (V64 v) -> Ll.Dot.dot_rec_nil k (vec_concise v) [] Vec_Double_t
    Udf_K _ ->
      if dot_draw_constant_nodes
        then Ll.Dot.dot_rec_nil k (udf_concise u) [] (udf_type_of u)
        else "/* node = constant */"
    Udf_P nm ty i -> Ll.Dot.dot_rec_nil k nm (dot_arg i) ty
    Udf_MemRd _ x -> Ll.Dot.dot_rec_nil k (udf_concise u) [Right x] (k_df_type_of x)
    Udf_MemWr _ _ x -> Ll.Dot.dot_rec_nil k (udf_concise u) (dot_arg [x]) (udf_type_of x)
    Udf_Dmd _ tr gr -> Ll.Dot.dot_rec_nil k "df_demand" (dot_arg (tr : gr)) (udf_type_of u)
    Udf_Ref _ _ -> Ll.Dot.dot_rec_nil k (udf_concise u) [] (udf_type_of u)
    Udf_Mrg _ _ -> error "dot_node: MRG"

-- | Dot notation of 'Edge'.
dot_edge :: [Node] -> Edge -> String
dot_edge n (j, k, p) =
  let e = printf "%d:o_0 -> %d:i_%d" j k p
  in case lookupUdf n j of
      Udf_K _ -> if dot_draw_constant_nodes then e else "/* lhs = constant */"
      _ -> e

-- > dot_cluster "x" ["a","b","c"] == "subgraph cluster_x {a b c};"
dot_cluster :: String -> [String] -> String
dot_cluster nm k = printf "subgraph cluster_%s {%s};" nm (unwords k)

-- | Dot notation of 'Graph'.
dot_graph :: Bool -> Graph -> [String]
dot_graph draw_sg g =
  let d =
        if draw_sg
          then adj_graph_demand_subgraphs (graph_to_adj_graph g)
          else []
      (n, e) = g
      mk_subgraph (k, k_set) = dot_cluster (show k) (map show k_set)
  in concat
      [
        [ "digraph Anonymous {"
        , "graph [splines=false];"
        ]
      , map dot_node n
      , map mk_subgraph d
      , map (dot_edge (fst g)) e
      , ["}"]
      ]

-- | View dot graph.
dot_draw :: String -> IO ()
dot_draw s = do
  t <- getTemporaryDirectory
  v <- fmap (fromMaybe "dotty") (lookupEnv "DOTVIEWER")
  let fn = t </> "udf" <.> "dot"
  writeFile fn s
  _ <- rawSystem v [fn]
  return ()

-- | Draw graph, transformed by `vgraph_direct`.
udf_to_dot :: Udf -> String
udf_to_dot = unlines . dot_graph False . udf_to_graph

draw :: Udf -> IO ()
draw = dot_draw . udf_to_dot

-- * Gr Drawing

-- | FGL graph with pretty-printed 'Udf' label.
type Gr_PP = Graph.Gr String PortIndex

graph_to_gr_pp :: Graph -> Gr_PP
graph_to_gr_pp (n, e) = Graph.mkGraph (map (fmap udf_concise) n) e

-- | Make @dot@ rendering of graph at 'Node', via 'vgraph_direct'.
gr_udf_to_dot :: Udf -> String
gr_udf_to_dot = Graph.showDot . Graph.fglToDot . graph_to_gr_pp . udf_to_graph

-- | Draw graph, via 'gr_dot'.
gr_draw :: Udf -> IO ()
gr_draw = dot_draw . gr_udf_to_dot
