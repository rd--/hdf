{- | Interaction with @jack-dl@, @scsynth@ and @text-dl@.
See <http://rd.slavepianos.org/?t=rju>.
-}
module Sound.Df.Ll.Audition where

import System.Directory {- directory -}
import System.FilePath {- filepath -}
import System.Process {- process -}

import qualified Sound.Osc as Osc {- hosc -}
import qualified Sound.Sc3 as Sc3 {- hsc3 -}
import qualified Sound.Sc3.Ugen.Bindings.Hw.Construct as Sc3 {- hsc3 -}

import qualified Sound.Sc3.Common.Uid as Uid {- hsc3 -}

import qualified Sound.Df.Ll.Cgen as Cgen
import qualified Sound.Df.Ll.Command as Command

-- import Sound.Sc3.Ugen.External.RDU {- sc3-rdu -}

-- | Local definition of RDL Ugen, to avoid dependency on sc3-rdu.
rdl :: Int -> Sc3.Ugen -> Sc3.Ugen
rdl nc i = Sc3.mkOscMCE Sc3.AudioRate "RDL" [] i nc

-- * jack-dl (rju)

-- | Run action with link to @jack-dl@.
with_jack_dl :: Osc.Connection Osc.OscSocket a -> IO a
with_jack_dl = Osc.withTransport (Osc.openOscSocket (Osc.Udp, "127.0.0.1", 57190))

-- | Audition graph after sending initialisation messages.
audition_rju :: [Osc.Message] -> Cgen.Instructions -> IO ()
audition_rju is ins = do
  t <- getTemporaryDirectory
  k <- Uid.generateUid
  let fn = t </> ("audition" ++ show k)
  Cgen.dl_gen fn (Cgen.JACK_DL, "/home/rohan/opt") ins
  with_jack_dl
    ( mapM Osc.sendMessage is
        >> Osc.sendMessage (Command.g_load (fn <.> "so"))
    )

-- * scsynth

-- | Load graph.
u_cmd_g_load :: Int -> Int -> String -> Osc.Message
u_cmd_g_load nid uid s = Sc3.u_cmd nid uid "/g_load" [Osc.string s]

-- | Audition graph after sending initialisation messages.
audition_sc3 :: [Osc.Message] -> Cgen.Instructions -> IO ()
audition_sc3 is ins = do
  t <- getTemporaryDirectory
  k <- Uid.generateUid
  let fn = t </> ("audition" ++ show k)
  Cgen.dl_gen fn (Cgen.Sc3_RDL, "/home/rohan/opt") ins
  Sc3.withSc3
    ( mapM Osc.sendMessage is
        >> Sc3.play (Sc3.out 0 (rdl 2 0))
        >> Osc.sendMessage (u_cmd_g_load (-1) 0 (fn <.> "so"))
    )

-- * text-dl

-- | Audition at @text-dl@.
audition_text :: Int -> Cgen.Instructions -> IO ()
audition_text nf ins = do
  t <- getTemporaryDirectory
  k <- Uid.generateUid
  let fn = t </> ("audition" ++ show k)
  Cgen.dl_gen fn (Cgen.TEXT_DL, "/home/rohan/opt") ins
  _ <- rawSystem "text-dl" ["-f", show nf, fn <.> "so"]
  return ()
