{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE FlexibleInstances #-}

-- | Data flow wire values.
module Sound.Df.Ll.K where

import Data.IORef {- base -}
import Data.Int {- base -}
import Data.Maybe {- base -}
import Data.Typeable {- base -}
import System.IO.Unsafe {- base -}

import System.Random {- random -}

-- * Mem

-- | Vector identifier.
type MemId = Int

{-# NOINLINE memIdMemory #-}
memIdMemory :: IORef MemId
memIdMemory = unsafePerformIO (newIORef 0)

-- | Generate next sequential MemId
nextMemId :: IO MemId
nextMemId = do
  z <- readIORef memIdMemory
  writeIORef memIdMemory (z + 1)
  return z

-- | Reset memIdMemory to zero.
memIdReset :: IO ()
memIdReset = writeIORef memIdMemory 0

-- * Vec

-- | Vector identifier.
type VecId = Int

-- | VecId for 8192 place sin table.
vecIdSin8192 :: VecId
vecIdSin8192 = 0

vecIdUserSpace :: VecId
vecIdUserSpace = 1

{-# NOINLINE vecIdMemory #-}
vecIdMemory :: IORef VecId
vecIdMemory = unsafePerformIO (newIORef vecIdUserSpace)

-- | Generate next sequential VecId
nextVecId :: IO VecId
nextVecId = do
  i <- readIORef vecIdMemory
  writeIORef vecIdMemory (i + 1)
  return i

liftVec1 :: (VecId -> a -> r) -> a -> IO r
liftVec1 f a = do
  z <- nextVecId
  return (f z a)

liftVec2 :: (VecId -> a -> b -> r) -> a -> b -> IO r
liftVec2 f a b = do
  z <- nextVecId
  return (f z a b)

liftVec3 :: (VecId -> a -> b -> c -> r) -> a -> b -> c -> IO r
liftVec3 f a b c = do
  z <- nextVecId
  return (f z a b c)

-- | Vector type (VectorIdentifier, VectorSize, VectorData).
data Vec a = Vec VecId Int [a] deriving (Typeable, Eq, Ord, Read, Show)

-- | Table identifier, alias for VecId where the Vec is a Tbl.
type TblId = VecId

-- | Table type, alias for Vec where the table has a guard point.
type Tbl t = Vec t

-- | 'VecId' of 'Vec'.
vecId :: Vec t -> VecId
vecId (Vec k _ _) = k

{- | Concise pretty printer and 'Show' instance for 'Vec'.

> vec_concise (Vec 0 1 [0]) == "vec(0,1)"
-}
vec_concise :: Vec a -> String
vec_concise (Vec k n _) = concat ["vec(", show k, ",", show n, ")"]

-- * K

{- | Sum type for wire values.
N = nil, B = boolean, I = integer, F = floating point, V = vector of F (array).
-}
data K_Ty
  = N ()
  | B Bool
  | I32 Int32
  | I64 Int64
  | F32 Float
  | F64 Double
  | V32 (Vec Float)
  | V64 (Vec Double)
  deriving (Eq, Typeable)

{- | 'Typeable' instance for 'K_Ty'.

map k_typeOf [B False,I32 0,F32 0.0,F64 1.0] == [bool_t,int32_t,float_t,double_t]
-}
k_typeOf :: K_Ty -> TypeRep
k_typeOf k =
  case k of
    N () -> nil_t
    B _ -> bool_t
    I32 _ -> int32_t
    I64 _ -> int64_t
    F32 _ -> float_t
    F64 _ -> double_t
    V32 _ -> vec_float_t
    V64 _ -> vec_double_t

k_df_type_of :: K_Ty -> Df_Type
k_df_type_of = df_typerep_type . k_typeOf

-- | Concise pretty printer and 'Show' instance for 'K'.
k_concise :: K_Ty -> String
k_concise k =
  case k of
    N () -> "()"
    B b -> show b
    I32 i -> show i
    I64 i -> show i
    F32 f -> show f
    F64 f -> show f
    V32 v -> vec_concise v
    V64 v -> vec_concise v

instance Show K_Ty where show = k_concise

-- * TypeRep constants

-- | 'typeOf' @()@.
nil_t :: TypeRep
nil_t = typeOf ()

-- | 'typeOf' of 'Bool'.
bool_t :: TypeRep
bool_t = typeOf (undefined :: Bool)

-- | 'typeOf' of 'Int32'.
int32_t :: TypeRep
int32_t = typeOf (undefined :: Int32)

-- | 'typeOf' of 'Int64'.
int64_t :: TypeRep
int64_t = typeOf (undefined :: Int64)

-- | 'typeOf' of 'Float'.
float_t :: TypeRep
float_t = typeOf (undefined :: Float)

-- | 'typeOf' of 'Double'.
double_t :: TypeRep
double_t = typeOf (undefined :: Double)

-- | 'typeOf' of ('Vec' 'Float').
vec_float_t :: TypeRep
vec_float_t = typeOf (undefined :: Vec Float)

-- | 'typeOf' of ('Vec' 'Double').
vec_double_t :: TypeRep
vec_double_t = typeOf (undefined :: Vec Double)

-- * Df Type

-- | Set of allowed Df types.
data Df_Type
  = Nil_t
  | Bool_t
  | Int32_t
  | Int64_t
  | Float_t
  | Double_t
  | Vec_Float_t
  | Vec_Double_t
  deriving (Eq, Enum, Bounded, Read, Show)

-- | Table of allowed Df types with their TypeRep.
df_type_table :: [(TypeRep, Df_Type)]
df_type_table =
  zip
    [nil_t, bool_t, int32_t, int64_t, float_t, double_t, vec_float_t, vec_double_t]
    [minBound .. maxBound]

df_typerep_type :: TypeRep -> Df_Type
df_typerep_type = fromMaybe (error "df_type_of") . flip lookup df_type_table

-- * Rng

-- | Type to indicate random-number-generator seed.
newtype Rng32 = Rng32 Int32 deriving (Eq, Ord, Read, Show)

-- | Type to indicate random-number-generator seed.
newtype Rng64 = Rng64 Int64 deriving (Eq, Ord, Read, Show)

-- | Generate random Rng64.
generateRng64 :: IO Rng64
generateRng64 = do
  i <- randomIO
  return (Rng64 i)

-- * Type classes

-- | Class for values that can be lifted to 'K'.
class (Typeable a, Eq a, Ord a, Read a, Show a) => K a where
  to_k :: a -> K_Ty

instance K () where to_k () = N ()
instance K Bool where to_k b = B b
instance K Int32 where to_k i = I32 i
instance K Int64 where to_k i = I64 i
instance K Rng32 where to_k (Rng32 i) = I32 i
instance K Rng64 where to_k (Rng64 i) = I64 i
instance K Float where to_k f = F32 f
instance K Double where to_k f = F64 f
instance K (Vec Float) where to_k v = V32 v
instance K (Vec Double) where to_k v = V64 v

-- | Composite of 'Ord' and `K`.
class (K a, Ord a) => K_Ord a

instance K_Ord Bool
instance K_Ord Int64
instance K_Ord Double

-- | Composite of 'K_Ord' and 'Num'.
class (K_Ord a, Num a) => K_Num a

instance K_Num Int64
instance K_Num Double
