-- | C code generator
module Sound.Df.Ll.Cgen where

import Data.Char {- base -}
import Data.Int {- base -}
import Data.List {- base -}
import Data.Maybe {- base -}
import System.Environment {- base -}
import System.FilePath {- filepath -}
import System.Process {- process -}

import qualified Sound.Sc3.Common.Uid as Uid {- hsc3 -}

import Sound.Df.Ll.K

-- * Types

-- | Typed variable value.
data Var_Fld
  = Var_F32 Float
  | Var_F64 Double
  | Var_V32 [Float]
  | Var_V64 [Double]
  | Var_B Bool
  | Var_I32 Int32
  | Var_I64 Int64

-- | (Type,Array,Label/Id,Initialised)
type Var = (Var_Ty, Df_Type, Uid.Id, Maybe Var_Fld)

-- | Enumeration of variable types.
data Var_Ty -- Rec_Var |
  = Dmd_Var Int
  | Std_Var
  | Buf_Var Int
  | Mem_Var MemId
  deriving (Eq, Show)

-- | C comment.
type C_Comment = String

-- | C type.
type C_Type = String

-- | Qualified name, (structure,access,member).
type C_QName = (String, String, String)

type C_Var = (Var_Ty, Uid.Id)

-- | C function/macro call.  (comment?,function?,arguments,end)
type C_Call = (Maybe String, Maybe String, [C_Var], String)

-- | List of constants, list of variables, list of c-calls (init,dsp).
type Instructions = ([(Uid.Id, K_Ty)], [Var], ([C_Call], [C_Call]))

-- | Enumeration of code hosts.
data Host = JACK_DL | Sc3_RDL | TEXT_DL

-- * C init / call

{- | Add comment markers.

> c_comment "c" == "/* c */"
-}
c_comment :: String -> C_Comment
c_comment c = concat ["/* ", c, " */"]

{- | Translate 'Df_Type' to 'C_Type'.

> c_df_type_to_ctype bool_t == "bool"
> c_df_type_to_ctype (typeOf (0.0::Float)) == "float"
-}
c_df_type_to_ctype :: Df_Type -> C_Type
c_df_type_to_ctype t =
  let tbl =
        [ (Bool_t, "bool")
        , (Int32_t, "int32_t")
        , (Int64_t, "int64_t")
        , (Float_t, "float")
        , (Double_t, "double")
        ]
  in fromMaybe (error (show ("c_df_type_to_ctype", t))) (lookup t tbl)

-- | Print initial value for Var_Fld.
var_fld_initialiser :: Var_Fld -> String
var_fld_initialiser v =
  case v of
    Var_B b -> map toLower (show b)
    Var_I32 i -> show i
    Var_I64 i -> show i
    Var_F32 f -> show f
    Var_F64 f -> show f
    Var_V32 _ -> error "var_fld_initialiser: vector"
    Var_V64 _ -> error "var_fld_initialiser: vector"

{- | Initialise 'C_QName' to value.

> c_init_atom ("s",".","r") 5 == "s.m = 5;"
-}
c_init_atom :: C_QName -> Var_Fld -> String
c_init_atom (s, a, p) q = concat [s, a, p, " = ", var_fld_initialiser q, ";"]

{- | Initialise 'C_QName' to array.  Generates loop code for sequences
of equal initial values.

> c_init_vec ("s",".","r") [0,1] == ["s.r[0] = 0;"
>                                   ,"s.r[1] = 1;"]

> let r = ["for(int i=0;i < 2;i++) {s.r[i] = 0;}"]
> in c_init_vec ("s",".","r") [0,0] == r
-}
c_init_vec :: (Eq a, Show a) => C_QName -> [a] -> [String]
c_init_vec (s, a, n) l =
  let init_arr_1 p q r = concat [s, a, p, "[", q, "] = ", show r, ";"]
      init_arr p q r =
        [ "for(int32_t i="
        , show q
        , ";i < "
        , show (q + length r)
        , ";i++) {"
        , init_arr_1 p "i" (head r)
        , "}"
        ]
      f (k, i) = case i of
        [i'] -> init_arr_1 n (show k) i'
        _ -> concat (init_arr n k i)
      l' = group l
  in map f (zip (dx_d (map length l')) l')

{- | Initialise 'C_QName' to value or array.

> let {qn = ("s","->","r")
>     ;r = ["for(int i=0;i < 2;i++) {s->r[i] = 0;}","s->r[2] = 1;"]}
> in c_init_var qn (Right [0,0,1]) == r
-}
c_init_var :: C_QName -> Var_Fld -> [String]
c_init_var qn e =
  case e of
    Var_B _ -> [c_init_atom qn e]
    Var_I32 _ -> [c_init_atom qn e]
    Var_I64 _ -> [c_init_atom qn e]
    Var_F32 _ -> [c_init_atom qn e]
    Var_F64 _ -> [c_init_atom qn e]
    Var_V32 [] -> error "c_init_var: Right []"
    Var_V32 l -> c_init_vec qn l
    Var_V64 [] -> error "c_init_var: Right []"
    Var_V64 l -> c_init_vec qn l

{- | Qualify name if required.  The /rf/ flag indicates if array is a
reference or an allocation.

> c_array_qual (Vec_Port float_t 3) "a" True == "*a"
> c_array_qual (Vec_Port float_t 3) "a" False == "a[3]"
-}
c_array_qual :: Maybe Int -> String -> Bool -> String
c_array_qual vc nm rf =
  case vc of
    Nothing -> nm
    Just n ->
      if rf
        then '*' : nm
        else nm ++ bracket ('[', ']') (show n)

{- | Construct a function/macro call, or control flow marker.

> c_call (Just "c",Just "fn",[(Std_Var,0)],";") == "fn(m.n_0); /* c */"
> c_call (Nothing,Just "if",[(Std_Var,0)],"{") == "if(m.n_0){"
> c_call (Just "endif",Nothing,[],"}") == "} /* endif */"
-}
c_call :: C_Call -> String
c_call (com, s, as, end) =
  let s' = maybe "" id s
      as' =
        if null as
          then ""
          else concat ["(", intercalate "," (map m_clabel as), ")"]
      com' = maybe "" ((' ' :) . c_comment) com
  in concat [s', as', end, com']

-- * Variables

-- | The character prefix for a 'Var' name is given by the 'Var_Ty'.
var_ty_char :: Var_Ty -> Char
var_ty_char ty =
  case ty of
    Mem_Var _ -> 'm'
    -- Rec_Var -> 'r'
    Dmd_Var _ -> 'd'
    Std_Var -> 'n'
    Buf_Var _ -> 'n' -- this is 'n' instead of 'b' because the implementation is poor ; at times Buf_Var are stored as Std_Var...

-- | The index suffix for a 'Var' name is given by the 'Var_Ty'.
var_ty_index :: Var_Ty -> Maybe Int
var_ty_index ty =
  case ty of
    Dmd_Var n -> Just n
    _ -> Nothing

-- | 'Var' name.
var_nm :: Var -> String
var_nm (vc, _, k, _) = clabel (vc, k)

-- | All non-'Std_Var' are stateful, ie. 'Dmd_Var' and 'Mem_Var' and 'Buf_Var'.
is_stateful :: Var -> Bool
is_stateful (vt, _, _, _) = vt /= Std_Var

-- | 'Mem_Var' and 'Dmd_Var' are stateful and /atom/s.
is_stateful_atom :: Var -> Bool
is_stateful_atom (vt, _, _, _) =
  case vt of
    -- Rec_Var -> True
    Dmd_Var _ -> True
    Mem_Var _ -> True
    _ -> False

-- | Lift K_Ty to Var_Fld, if possible.
k_to_var_fld :: K_Ty -> Var_Fld
k_to_var_fld n =
  case n of
    N () -> error "var_fld: ()"
    B b -> Var_B b
    I32 i -> Var_I32 i
    I64 i -> Var_I64 i
    F32 f -> Var_F32 f
    F64 f -> Var_F64 f
    V32 _v -> error "var_fld: Vec Float" -- Var_V32 v
    V64 _v -> error "var_fld: Vec Double" -- Var_V64 v

-- | Generate 'Var' from 'K'.
k_var :: Uid.Id -> Var_Ty -> K_Ty -> Var
k_var k vt n =
  case n of
    N _ -> error "k_var: ()"
    B b -> (vt, Bool_t, k, Just (Var_B b))
    I32 i -> (vt, Int32_t, k, Just (Var_I32 i))
    I64 i -> (vt, Int64_t, k, Just (Var_I64 i))
    F32 f -> (vt, Float_t, k, Just (Var_F32 f))
    F64 f -> (vt, Double_t, k, Just (Var_F64 f))
    V32 _ -> error "k_var: Vec Float"
    V64 (Vec _ m l) -> (Buf_Var m, Double_t, k, Just (Var_V64 l))

{-
buffer_var :: Uid.Id -> K_Ty -> Var
buffer_var k n =
  case n of
    V64 (Vec _ m l) -> (Buf_Var m,double_t,k,Just (Var_V64 l))
    _ -> error "buffer_var?"
-}

-- | 'c_init_var' of 'Var'.
var_init :: String -> String -> Var -> [String]
var_init s a (vt, _, k, i) =
  case i of
    Nothing -> error (show ("var_init", s, a, vt, k))
    Just i' -> c_init_var (s, a, clabel (vt, k)) i'

-- | 'Var' C declaration, /rf/ determines 'c_array_qual' form.
var_decl :: Bool -> Var -> String
var_decl rf (vt, ty, k, _) =
  let vc = case vt of
        Buf_Var n -> Just n
        _ -> Nothing
      nm = clabel (vt, k)
  in c_df_type_to_ctype ty ++ " " ++ c_array_qual vc nm rf ++ ";"

{- | Generate a C @struct@ for 'Var', predicate determines if array
variables are refernces or allocations.
-}
gen_var_struct :: String -> (Var -> Bool) -> [Var] -> [String]
gen_var_struct nm f vs =
  let dc = zipWith var_decl (map f vs) vs
  in c_comment nm
      : bracket ("struct " ++ nm ++ " {", "};") dc

{- | Construct an identifier.  Mem vars are named using MemId, not Label.

> map clabel [(Std_Var,0),(Dmd_Var 1,1),(Buf_Var 2,2)] == ["n_0","d_1_1","b_2"]
-}
clabel :: C_Var -> String
clabel (ty, k) =
  let z = case ty of
        Mem_Var memid -> memid
        _ -> k
  in (var_ty_char ty : '_' : show z) ++ maybe "" (('_' :) . show) (var_ty_index ty)

{- | 'clabel' of 'Std_Var'.

> std_clabel 0 == "n_0"
-}
std_clabel :: Uid.Id -> String
std_clabel k = clabel (Std_Var, k)

-- | Variant with @m.@ prefix.
m_clabel :: C_Var -> String
m_clabel = ("m." ++) . clabel

{- | 'c_init_var' for constant.

> c_const (0,I 1) == ["m.n_0 = 1;"]
-}
c_const :: (Uid.Id, K_Ty) -> [String]
c_const (k, v) =
  case v of
    B x -> c_init_var ("m", ".", std_clabel k) (Var_B x)
    I32 x -> c_init_var ("m", ".", std_clabel k) (Var_I32 x)
    I64 x -> c_init_var ("m", ".", std_clabel k) (Var_I64 x)
    F32 x -> c_init_var ("m", ".", std_clabel k) (Var_F32 x)
    F64 x -> c_init_var ("m", ".", std_clabel k) (Var_F64 x)
    V64 _ -> [] -- see buffer_var
    _ -> error "c_const: k"

-- * Code generators

-- | C declarations for DSP functions (memreq,init and step).
dsp_fun_decl :: [String]
dsp_fun_decl =
  [ "size_t dsp_memreq();"
  , "void dsp_init(void *p);"
  , "void dsp_step(df_world *w,int32_t w_nf);"
  ]

{- | The structure for all memory stores.  In the uniform model this
is a notational convenience only.  In a partioned model it is
functional.
-}
cmem :: [Var] -> [String]
cmem = gen_var_struct "df_mem" is_stateful

-- | The structure for stateful 'Var'.
cstate :: [Var] -> [String]
cstate = gen_var_struct "df_state" (const False) . filter is_stateful

-- | Generate dsp_memreq function.
dsp_memreq :: [String]
dsp_memreq =
  [ "size_t dsp_memreq()"
  , "{"
  , "return (sizeof(struct df_state));"
  , "}"
  ]

-- | Generate dsp_init function.
dsp_init :: [Var] -> [String]
dsp_init vs =
  let a =
        [ "void dsp_init(void *p)"
        , "{"
        ]
      b =
        [ "return;"
        , "}"
        ]
      c = case filter is_stateful vs of
        [] -> []
        vs' ->
          "struct df_state *s = (struct df_state *)p;"
            : concatMap (var_init "s" "->") vs'
  in a ++ c ++ b

-- | Generate @dsp_step@ function.
dsp_step :: Instructions -> [String]
dsp_step (ks, vs, (cc_init, cc_dsp)) =
  let f v = let nm = var_nm v in "m." ++ nm ++ " = s->" ++ nm ++ ";"
      g v = let nm = var_nm v in "s->" ++ nm ++ " = m." ++ nm ++ ";"
  in concat
      [
        [ "void dsp_step(df_world *w,int32_t w_nf)"
        , "{"
        , "struct df_mem m;"
        ]
      , let v = filter is_stateful vs
        in if null v
            then []
            else
              [ "struct df_state *s = (struct df_state*)w_state(w);"
              , "/* load state */"
              ]
                ++ map f v
      , ["/* constants */"]
      , concatMap c_const ks
      , ["/* block-initialisation-rate */"]
      , map c_call cc_init
      ,
        [ "/* algorithm */"
        , "for(int32_t fc = 0; fc < w_nf; fc++) {"
        ]
      , map c_call cc_dsp
      ,
        [ "}"
        , "/* store state */"
        ]
      , map g (filter is_stateful_atom vs)
      , ["}"]
      ]

-- | Generate C code for graph.
code_gen :: Host -> Instructions -> String
code_gen h (ks, vs, cc) =
  let hd =
        [ "#include <stdio.h>"
        , "#include <stdint.h>"
        , "#include <stdlib.h>"
        , "#include <stdbool.h>"
        , "#include <math.h>"
        , host_include h
        , "#include \"/home/rohan/sw/hdf/c/hdf.h\""
        ]
      c =
        [ hd
        , host_dsp_fun_decl h
        , cstate vs
        , cmem vs
        , dsp_memreq
        , dsp_init vs
        , dsp_step (ks, vs, cc)
        ]
  in (unlines . concat) c

-- * Host

-- | Host specific @#include@ file.
host_include :: Host -> String
host_include h =
  case h of
    JACK_DL -> "#include \"/home/rohan/sw/rju/cmd/rju-dl.h\""
    Sc3_RDL -> "#include \"/home/rohan/sw/sc3-rdu/cpp/RDL.h\""
    TEXT_DL -> "#include \"/home/rohan/sw/hdf/c/text-dl.h\""

-- | Host specific form of 'dsp_fun_decl' (@extern C@ where required).
host_dsp_fun_decl :: Host -> [String]
host_dsp_fun_decl h =
  case h of
    Sc3_RDL -> bracket ("extern \"C\" {", "}") dsp_fun_decl
    _ -> dsp_fun_decl

-- | Get CC and CXX environment variables, or defaults.
host_get_compiler_opt :: (String, String) -> IO (String, String)
host_get_compiler_opt (def_cc, def_cxx) = do
  cc <- fmap (fromMaybe def_cc) (lookupEnv "CC")
  cxx <- fmap (fromMaybe def_cxx) (lookupEnv "CXX")
  return (cc, cxx)

{- | Generate compiler command for 'Host' given @include@ directory prefix.

PIC is required for shared libraries,
fast-math sets CPU to treat denormals as zero.
-}
host_compiler_cmd :: (String, String) -> (Host, FilePath) -> (String, [String])
host_compiler_cmd (cc, cxx) (h, d) =
  let cppflags = ["-Wall", "-pedantic", "-g", "-O2", "-shared", "-fPIC", "-ffast-math"]
      cflags = ["-std=c99"]
      cxxflags = ["-std=c++11"]
  in case h of
      Sc3_RDL ->
        ( cxx
        , concat
            [ cppflags
            , cxxflags
            ,
              [ "-I"
              , d </> "include/SuperCollider/plugin_interface"
              , "-I"
              , d </> "include/SuperCollider/common"
              ]
            ]
        )
      _ ->
        ( cc
        , concat
            [ cppflags
            , cflags
            , ["-I", d </> "include"]
            ]
        )

{- | Format 'host_compiler_cmd' as 'String'.

> let mk ty = host_compiler_cmd_str ("gcc","g++") (ty,"/home/rohan/opt")
> map mk [JACK_DL,Sc3_RDL,TEXT_DL]
-}
host_compiler_cmd_str :: (String, String) -> (Host, FilePath) -> String
host_compiler_cmd_str opt = let f (cmd, arg) = unwords (cmd : arg) in f . host_compiler_cmd opt

-- * IO

{- | Generate C code, write file to disk and call the GNU C compiler
  to build shared library.
-}
dl_gen :: FilePath -> (Host, FilePath) -> Instructions -> IO ()
dl_gen fn (h, d) i = do
  (cc, cxx) <- host_get_compiler_opt ("gcc", "g++") -- ("gcc","g++") ("clang","clang++")
  let c = fn <.> "c"
      so = fn <.> "so"
      (cmd, opt) = host_compiler_cmd (cc, cxx) (h, d)
      opt' = opt ++ [c, "-o", so]
  writeFile c (code_gen h i)
  _ <- rawSystem cmd opt'
  return ()

-- * List

{- | Bracket list with elements.

> bracket ('<','>') "float" == "<float>"
-}
bracket :: (a, a) -> [a] -> [a]
bracket (i, j) k = i : k ++ [j]

{- | Integrate, with implicit @0@.

> dx_d [5,6] == [0,5,11]
-}
dx_d :: Num n => [n] -> [n]
dx_d = (0 :) . scanl1 (+)
