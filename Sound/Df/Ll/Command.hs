-- | Osc graph commands.
module Sound.Df.Ll.Command where

import qualified Sound.Osc.Core as Osc {- hosc -}

-- | Load graph.
g_load :: String -> Osc.Message
g_load s = Osc.Message "/g_load" [Osc.string s]

-- | Unload graph.
g_unload :: Osc.Message
g_unload = Osc.Message "/g_unload" []
