-- | Elementary dot.
module Sound.Df.Ll.Dot where

import Data.List {- base -}
import Data.Maybe {- base -}
import Text.Printf {- base -}

import qualified Sound.Sc3.Common.Uid as Uid {- hsc3 -}

import Sound.Df.Ll.K

{- | Map from 'Df_Type' to colour name.

> map (ty_colour . Just) [int64_t,double_t] == ["orange","blue"]
-}
ty_colour :: Maybe Df_Type -> String
ty_colour m =
  let tbl =
        [ (Bool_t, "brown")
        , (Int64_t, "orange")
        , (Double_t, "blue")
        , (Nil_t, "black")
        , (Vec_Double_t, "purple")
        ]
  in case m of
      Nothing -> "grey"
      Just ty -> fromMaybe "red" (lookup ty tbl)

{- | Left & right bracket.

> w_bracket '(' ')' "parentheses" == "(parentheses)"
-}
w_bracket :: a -> a -> [a] -> [a]
w_bracket p q l = p : l ++ [q]

-- | Dot notation for /key,value/ attributes.
dot_attr :: [(String, String)] -> String
dot_attr a =
  let in_quotes = w_bracket '"' '"'
      in_square = w_bracket '[' ']'
      sep_commas = intercalate ","
      join_eq p q = p ++ "=" ++ q
      (k, v) = unzip a
  in in_square (sep_commas (zipWith join_eq k (map in_quotes v)))

{- | Dot node as /record/.  Constant values are drawn directly into
input ports.  The /nm/ 'String' has the @df_@ prefix removed for
printing.

> dot_rec 0 "nm" [] (Just float_t)
-}
dot_rec :: Uid.Id -> String -> [Either Int K_Ty] -> Maybe Df_Type -> String
dot_rec k nm ar ty =
  let mk_i i = case i of
        Left i_k -> printf "<i_%d>" i_k
        Right i_k -> k_concise i_k
      ip =
        if null ar
          then ""
          else '|' : intercalate "|" (map mk_i ar)
      op = maybe "" (const "|<o_0>") ty
      nm' = fromMaybe nm (stripPrefix "df_" nm)
      c = ty_colour ty
      a =
        [ ("shape", "record")
        , ("color", c)
        , ("label", printf "{{%s%s%s}}" nm' ip op)
        ]
  in printf "%d %s;" k (dot_attr a)

-- | Make arguments input for 'dot_rec' from arity.
dot_rec_arity :: Int -> [Either Int K_Ty]
dot_rec_arity n = map Left [0 .. n - 1]

-- | Variant where 'nil_t' indicates no output.
dot_rec_nil :: Uid.Id -> String -> [Either Int K_Ty] -> Df_Type -> String
dot_rec_nil k nm n ty =
  let ty' = if ty == Nil_t then Nothing else Just ty
  in dot_rec k nm n ty'
