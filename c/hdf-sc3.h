/* simplified implementation of sc3 ugens - direct translation of graphdef files to c */

#include <math.h>
#include <stdio.h>

#include "c-common/signal-interpolate.c"

typedef int I;

typedef float R;
#define r_round roundf
#define r_tanh tanhf

#define TBL_SZ 8192
#define PI 3.141592653589793
#define TWO_PI 6.283185307179586

/* World */
typedef struct {
    R done_action;
    R sample_rate; /* hz */
    R sample_dur; /* seconds */
    R sin_tbl[TBL_SZ];
} W;

void w_init(W *w,R sr) {
    w->done_action = 0;
    w->sample_rate = sr;
    w->sample_dur = 1 / sr;
    R phase = 0;
    R phase_incr = TWO_PI / TBL_SZ;
    for(I i;i < TBL_SZ;i++) {
        w->sin_tbl[i] = sin(phase);
        phase += phase_incr;
    }
}

/* UnaryOp */
void neg_calc(R *o,R x) {*o = -x;}
void tanh_calc(R *o,R x) {*o = r_tanh(x);}

/* BinaryOp */
void add_calc(R *o,R x,R y) {*o = x + y;}
void sub_calc(R *o,R x,R y) {*o = x - y;}
void mul_calc(R *o,R x,R y) {*o = x * y;}
void fdiv_calc(R *o,R x,R y) {*o = x / y;}

/* Line */
typedef struct {R cur;I cnt;R dif;} line_t;

void line_ctor(W *w,line_t *u,R start,R end,R dur,R _act) {
    u->cur = start;
    u->cnt = (I)(dur * w->sample_rate);
    u->dif = (end - start) / (dur * w->sample_rate);
}

void line_calc(R *o,W *w,line_t *u,R _start,R _end,R _dur,R act) {
    *o = u->cur;
    if(u->cnt > 0) {
        u->cnt -= 1;
        u->cur += u->dif;
    }
    if(u->cnt == 1) {
        w->done_action = act;
    }
}

/* MulAdd */
void mul_add_calc(R *o,R x,R y,R z) {
    *o = x * y + z;
}

/* SinOsc */
typedef struct {R phase;} sin_osc_t;

void sin_osc_ctor(W *w,sin_osc_t *u,R freq,R iphase) {
    u->phase = iphase / TWO_PI;
}

void sin_osc_calc(R *o,W *w,sin_osc_t *u,R freq,R iphase) {
    *o = signal_interpolate(w->sin_tbl, TBL_SZ, u->phase * TBL_SZ);
    u->phase += freq * w->sample_dur;
    if(u->phase >= 1.0) {
        u->phase -= 1.0;
    }
}

int main (void) {
    R k[5] = {6,0,1,0.75,2};
    W w;
    w_init(&w,48000);
    sin_osc_t u1;
    sin_osc_ctor(&w,&u1,k[0],k[1]);
    line_t u2;
    line_ctor(&w,&u2,k[2],k[1],k[3],k[4]);
    R r1,r2,r3;
    while(w.done_action == 0.0) {
        sin_osc_calc(&r1,&w,&u1,k[0],k[1]);
        line_calc(&r2,&w,&u2,k[2],k[1],k[3],k[4]);
        mul_calc(&r3,r1,r2);
        printf("%f\n",r3);
    }
    return 0;
}
