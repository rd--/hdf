/* reader */
#define df_integer_constant(o_0,i_0) {o_0 = i_0;}
#define df_real_constant(o_0,i_0) {o_0 = i_0;}
#define df_rec_r(o_0,i_0) {o_0 = i_0;}
#define df_rec_w(o_0,i_0) {o_0 = i_0;}
/* instance Num */
#define df_add(o_0,i_0,i_1) {o_0 = (i_0) + (i_1);}
#define df_mul(o_0,i_0,i_1) {o_0 = (i_0) * (i_1);}
#define df_mul_add(o_0,i_0,i_1,i_2) {o_0 = (i_0) * (i_1) + (i_2);}
#define df_sub(o_0,i_0,i_1) {o_0 = (i_0) - (i_1);}
#define df_negate(o_0,i_0) {o_0 = -(i_0);}
#define df_abs(o_0,i_0) {o_0 = (i_0) < 0 ? -(i_0) : (i_0);}
#define df_fabs(o_0,i_0) {o_0 = fabs(i_0);}
#define df_labs(o_0,i_0) {o_0 = (int64_t)labs((long int)i_0);} /* gcc - 64bit arch */
#define df_signum(o_0,i_0) {o_0 = i_0 > 0 ? 1 (i_0 < 0 ? -1 : 0);}}
/* instance Integral */
#define df_imod(o_0,i_0,i_1) {o_0 = i_0 % i_1;}
#define df_idiv(o_0,i_0,i_1) {o_0 = (i_0) / (i_1);}
/* instance Fractional */
#define df_fmod(o_0,i_0,i_1) {o_0 = fmod(i_0,i_1);}
#define df_fdiv(o_0,i_0,i_1) {o_0 = (i_0) / (i_1);}
#define df_recip(o_0,i_0) {o_0 = 1.0 / i_0;}
/* instance Floating */
#define df_exp(o_0,i_0) {o_0 = exp(i_0);}
#define df_sqrt(o_0,i_0) {o_0 = sqrt(i_0);}
#define df_log(o_0,i_0) {o_0 = log(i_0);}
#define df_pow(o_0,i_0,i_1) {o_0 = pow(i_0,i_1);}
#define df_sin(o_0,i_0) {o_0 = sin(i_0);}
#define df_cos(o_0,i_0) {o_0 = cos(i_0);}
#define df_tan(o_0,i_0) {o_0 = tan(i_0);}
/* cast */
#define df_double_to_int64(o_0,i_0) {o_0 = (int64_t)i_0;}
#define df_int64_to_double(o_0,i_0) {o_0 = (double)i_0;}
/* identity */
#define df_identity(o_0,i_0) {o_0 = i_0;}
/* instance Ord */
#define df_lt(o_0,i_0,i_1) {o_0 = i_0 < i_1 ? true : false;}
#define df_lte(o_0,i_0,i_1) {o_0 = i_0 <= i_1 ? true : false;}
#define df_gt(o_0,i_0,i_1) {o_0 = i_0 > i_1 ? true : false;}
#define df_gte(o_0,i_0,i_1) {o_0 = i_0 >= i_1 ? true : false;}
#define df_max(o_0,i_0,i_1) {o_0 = i_0 > i_1 ? i_0 : i_1;}
#define df_min(o_0,i_0,i_1) {o_0 = i_0 < i_1 ? i_0 : i_1;}
/* instance Eq */
#define df_eq(o_0,i_0,i_1) {o_0 = i_0 == i_1 ? true : false;}
#define df_eq_approx(o_0,i_0,i_1,i_2) {o_0 = fabs(i_2 - i_1) < i_0 ? true : false;}
/* instance RealFrac */
#define df_floor(o_0,i_0) {o_0 = floor(i_0);}
#define df_ceil(o_0,i_0) {o_0 = ceil(i_0);}
#define df_round(o_0,i_0) {o_0 = round(i_0);}
#define df_lrint(o_0,i_0) {o_0 = (int64_t)lrint(i_0);}
/* instance Bits */
#define df_bw_and(o_0,i_0,i_1) {o_0 = i_0 & i_1;}
#define df_bw_or(o_0,i_0,i_1) {o_0 = i_0 | i_1;}
#define df_bw_not(o_0,i_0) {o_0 = ~ i_0;}
#define df_bw_test_bit(o_0,i_0,i_1) {o_0 = (bool)((i_0 >> i_1) & 1);}
/* Control */
#define df_and(o_0,i_0,i_1) {o_0 = i_0 && i_1 ? true : false;}
#define df_or(o_0,i_0,i_1) {o_0 = i_0 || i_1 ? true : false;}
#define df_not(o_0,i_0) {o_0 = i_0 ? false : true;}
#define df_select2(o_0,i_0,i_1,i_2) {o_0 = i_0 ? i_1 : i_2;}
#define df_copy(o_0,i_0) {o_0 = i_0;}
/* Vector */
#define df_vec_read(o_0,i_0,i_1) {o_0 = i_0[i_1];}
#define df_vec_write(i_0,i_1,i_2) {i_0[i_1] = i_2;}
/* SC3 */
#define df_zap_gremlins(o_0,i_1) {o_0 = (fabs(i_1) > 1e-15 && fabs(i_1) < 1e15) ? i_1 : 0;}
/* World|Environment Random Number Generator */
#define df_frand(o_0,i_1,i_2) {o_0 = w_frand(w,i_1,i_2);}
/* World|Environment Param */
#define df_sample_rate(o_0) {o_0 = w_sr(w);}
#define df_block_nframes(o_0) {o_0 = (int64_t)w_nf;}
/* World|Environment IO */
#define df_block_edge(o_0) {o_0 = fc == 0 ? true : false;}
#define df_buf_read(o_0,i_0,i_1) {o_0 = w_buf_read1(w,i_0,i_1);}
#define df_buf_write(i_0,i_1,i_2) {w_buf_write1(w,i_0,i_1,i_2);}
#define df_in1(o_0,i_0) {o_0 = w_in1(w,i_0,fc);}
#define df_out1(i_0,i_1) {w_out1(w,i_0,fc,i_1);}
#define df_out2(i_0,i_1,i_2) {w_out2(w,i_0,fc,i_1,i_2);}
#define df_out3(i_0,i_1,i_2,i_3) {w_out3(w,i_0,fc,i_1,i_2,i_3);}
#define df_ctl1(o_0,i_0) {o_0 = w_c_get1(w,i_0);}
