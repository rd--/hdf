#include <dlfcn.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "text-dl.h"

void usage(void)
{
  printf("Usage: text-dl [ options ] dsp-file\n");
  printf("    -b N : Number of buffers (default=8).\n");
  printf("    -f N : Number of frames (default=48000).\n");
  printf("    -k N : Number of controls (default=64).\n");
  printf("    -l N : Buffer size (uniform, default=48000).\n");
  printf("    -s N : Sample rate (default=48000).\n");
  exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
  size_t (*dsp_memreq)();
  void (*dsp_init)(void *);
  void (*dsp_step)(struct world *, int);
  void *gh;
  struct world w;
  int nf = 48000;
  int bl = 48000;
  int c;
  w.nb = 8;
  w.nk = 64;
  w.sr = 48000;
  while((c = getopt(argc, argv, "b:f:hk:s:")) != -1) {
    switch(c) {
    case 'b': w.nb = (int)strtol(optarg, NULL, 0); break;
    case 'f': nf = (int)strtol(optarg, NULL, 0); break;
    case 'h': usage(); break;
    case 'k': w.nk = (int)strtol(optarg, NULL, 0); break;
    case 'l': bl = (int)strtol(optarg, NULL, 0); break;
    case 's': w.sr = (int)strtof(optarg, NULL); break;
    }
  }
  if (optind >= argc) usage();
  char *fn = argv[optind];
  w.ctl = calloc(w.nk, sizeof(float));
  w.bl = calloc(w.nb, sizeof(int));
  w.bd = calloc(w.nb, sizeof(float*));
  for(int i = 0; i < w.nb; i++ ) {
    w.bl[i] = bl;
    w.bd[i] = calloc(w.bl[i],sizeof(float));
  }
  gh = dlopen(fn, RTLD_LAZY);
  if(!gh) {
    printf("text-dl: %s",dlerror());
    return EXIT_FAILURE;
  }
  dsp_memreq = dlsym(gh, "dsp_memreq");
  dsp_init = dlsym(gh, "dsp_init");
  dsp_step = dlsym(gh, "dsp_step");
  size_t k = dsp_memreq();
  w.st = malloc(k);
  dsp_init(w.st);
  dsp_step(&w,nf);
  return EXIT_SUCCESS;
}
