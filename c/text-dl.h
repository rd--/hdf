#include <stdbool.h>
#include <stdint.h>

struct world {
  void *st;                    /* graph state */
  double sr;                   /* sample rate */
  int32_t nk;                  /* number of controls */
  double *ctl;                 /* control data */
  int32_t nb;                  /* number of buffers */
  int64_t *bl;                 /* buffer sizes */
  float **bd;                  /* buffer data */
};

#define df_world struct world
#define w_state(w) (w)->st
#define w_sr(w) (w)->sr
#define w_c_get1(w,i) (w)->ctl[(i)]
#define w_c_set1(w,i,n) (w)->ctl[(i)]=(n)
#define w_in1(w,c,i) 0
#define w_out1(w,c,i,n) printf("%ld,%d: %f\n",c,i,n)
#define w_out2(w,c,i,n1,n2) printf("%ld,%d: %f, %f\n",c,i,n1,n2)
#define w_out3(w,c,i,n1,n2,n3) printf("%ld,%d: %f, %f, %f\n",c,i,n1,n2,n3)
#define w_out4(w,c,i,n1,n2,n3,n4) printf("%ld,%d: %f, %f, %f, %f\n",c,i,n1,n2,n3,n4)
#define w_b_read1(w,b,i) (w)->bd[(b)][(i)]
#define w_b_write1(w,b,i,n) (w)->bd[(b)][(i)]=(n)
