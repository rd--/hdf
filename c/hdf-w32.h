/* Num */
#define df_abs(o_0,i_0) {o_0 = (int32_t)abs((int32_t)i_0);}

/* Float */
#define df_fmodf(o_0,i_0,i_1) {o_0 = fmodf(i_0,i_1);}
#define df_fabsf(o_0,i_0) {o_0 = fabsf(i_0);}
#define df_expf(o_0,i_0) {o_0 = expf(i_0);}
#define df_sqrtf(o_0,i_0) {o_0 = sqrtf(i_0);}
#define df_logf(o_0,i_0) {o_0 = logf(i_0);}
#define df_powf(o_0,i_0,i_1) {o_0 = powf(i_0,i_1);}
#define df_sinf(o_0,i_0) {o_0 = sinf(i_0);}
#define df_cosf(o_0,i_0) {o_0 = cosf(i_0);}
#define df_tanf(o_0,i_0) {o_0 = tanf(i_0);}
#define df_float_to_int32(o_0,i_0) {o_0 = (int32_t)i_0;}
#define df_int32_to_float(o_0,i_0) {o_0 = (float)i_0;}
#define df_floorf(o_0,i_0) {o_0 = floorf(i_0);}
#define df_ceilf(o_0,i_0) {o_0 = ceilf(i_0);}
#define df_roundf(o_0,i_0) {o_0 = roundf(i_0);}
#define df_lrintf(o_0,i_0) {o_0 = (int32_t)lrintf(i_0);}
#define df_zap_gremlinsf(o_0,i_1) {o_0 = (fabsf(i_1) > 1e-15 && fabsf(i_1) < 1e15) ? i_1 : 0;}
