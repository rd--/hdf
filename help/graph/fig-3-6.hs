-- Non-working...
import Data.Int {- base -}

import Sound.Df {- hdf -}

-- | Df variant of Faust @~@.
tilde :: (Num a,K a) => (Df a -> Df a) -> (Df a -> Df a) -> Df a
tilde f g = rec_h 0 (\i -> let r = f i in (r,g r))

-- | Df translation of Faust graph @(12345 : +) ~ (1103515245 : *)@.
--
-- > draw fig_3_6
fig_3_6 :: Df Int64
fig_3_6 = (12345 +) `tilde` (* 1103515245)

-- | Converted to floating point and sent to output.
--
-- > draw fig_3_6'
fig_3_6' :: Df ()
fig_3_6' = out1 0 (i64_to_normal_f64 fig_3_6 * 0.1)

main :: IO ()
main = audition_rju [] fig_3_6'
