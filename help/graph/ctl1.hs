import Sound.Osc {- hosc -}
import qualified Sound.Sc3 as Sc3 {- hsc3 -}

import Sound.Df {- hdf -}

-- > import qualified Sound.Df.Ll as Ll {- hdf -}
-- > Ll.with_jack_dl (mapM_ sendMessage ctl_osc_msg)
ctl_osc_msg :: [Message]
ctl_osc_msg = [Sc3.c_set1 0 440,Sc3.c_set1 1 0.1]

-- > draw ctl_osc
ctl_osc :: Df ()
ctl_osc =
    let o = sinosc (ctl1 0) 0.0 * ctl1 1
    in out1 0 o

-- > Ll.with_jack_dl (mapM_ sendMessage [Sc3.c_set1 0 880,Sc3.c_set1 1 0.1])
-- > Ll.with_jack_dl (mapM_ sendMessage [Sc3.c_set1 0 220,Sc3.c_set1 1 0.2])
main :: IO ()
main = audition_rju ctl_osc_msg ctl_osc

{-
import qualified Sound.Df.Gadt.Ugen.Monadic as M {- hdf -}
-- > drawM ctl_osc_m
-- > ctl_osc_m >>= audition_rju ctl_osc_msg
ctl_osc_m :: (Functor m,Sc3.Uid m) => m (Df ())
ctl_osc_m = do
  o <- fmap (* (ctl1 1)) (M.sin_osc_m (ctl1 0) 0.0)
  return (out1 0 o)
-}
