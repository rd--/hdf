-- Non-working...
import Data.Int {- base -}

import Sound.Df {- hdf -}

-- | Df variant of Faust @~@.
tilde_m :: (UId m,Num a,K a) => (Df a -> Df a) -> (Df a -> Df a) -> m (Df a)
tilde_m f g = rec_m 0 (\i -> let r = f i in (r,g r))

-- | Df translation of Faust graph @(12345 : +) ~ (1103515245 : *)@.
--
-- > drawM fig_3_6_m
fig_3_6_m :: (Functor m,UId m) => m (Df Int32)
fig_3_6_m = (+ 12345) `tilde_m` (* 1103515245)

-- | Converted to floating point and sent to output.
--
-- > drawM fig_3_6_m'
fig_3_6_m' :: (Functor m,UId m) => m (Df ())
fig_3_6_m' = fmap (out1 0 . (* 0.1) . i32_to_normal_f32) fig_3_6_m

main :: IO ()
main = audition_rju [] =<< fig_3_6_m'
