import Control.Monad {- base -}

import Sound.DF {- hdf -}

-- > drawM moto_rev_m
moto_rev_m :: (Functor m,UId m) => m (DF ())
moto_rev_m = do
  let dpl f a b = (,) (f a) (f b)
      madd m a = fmap ((+ a) . (* m))
      dplm f a b = liftM2 (,) (f a) (f b)
  f <- madd 10.0 21.0 (sin_osc_m 0.2 0.0)
  (s1, s2) <- dplm (\x -> lf_pulse_m f x 0.1) 0.0 0.1
  (o1, o2) <- dplm (\x -> rlpf_m x 100.0 0.1) s1 s2
  let (c1, c2) = dpl (\x -> df_clip2 x 0.4) o1 o2
  return (out2 0 c1 c2)

main :: IO ()
main = audition_rju [] =<< moto_rev_m
