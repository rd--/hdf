import Sound.DF {- hdf -}

k_sec = k_seconds_to_frames 48000

-- > draw lfo_modulation
lfo_modulation :: DF ()
lfo_modulation =
    let dpl f p q = (f p,f q)
        mk_p f = sin_osc f 0.0 * 3600.0 + 4000.0
        s = sin_osc 0.05 0.0 * 80.0 + 160.0
        (p1,p2) = dpl mk_p 0.6 0.7
        l = lf_pulse s 0 0.4 * 0.05
        (r1,r2) = dpl (\x -> rlpf l x 0.2) p1 p2
        c1 = comb_n_P (V_Id 0) (k_sec 0.20) r1 0.20 2.0
        c2 = comb_n_P (V_Id 1) (k_sec 0.25) r2 0.25 2.0
    in out2 0 c1 c2

main :: IO ()
main = audition_rju [] lfo_modulation
