import Sound.OSC {- hosc -}
import qualified Sound.SC3 as SC3 {- hsc3 -}

import Sound.DF {- hdf -}

-- > audition_rju [] (out1 0 0)
main :: IO ()
main = audition_rju lfo_modulation_buf_msg lfo_modulation_buf

lfo_modulation_buf_msg :: [Message]
lfo_modulation_buf_msg = [SC3.b_alloc 0 44100 1, SC3.b_alloc 1 44100 1]

-- > draw lfo_modulation_buf
lfo_modulation_buf :: DF ()
lfo_modulation_buf =
    let dpl f p q = (f p,f q)
        mk_p f = sin_osc f 0.0 * 3600.0 + 4000.0
        s = sin_osc 0.05 0.0 * 80.0 + 160.0
        (p1,p2) = dpl mk_p 0.6 0.7
        l = lf_pulse s 0 0.4 * 0.05
        (r1,r2) = dpl (\x -> rlpf l x 0.2) p1 p2
        c1 = buf_comb_n 0 r1 0.20 2.0
        c2 = buf_comb_n 1 r2 0.25 2.0
    in out2 0 c1 c2
