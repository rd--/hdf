import Control.Monad {- base -}

import Sound.OSC {- hosc -}
import qualified Sound.SC3 as SC3 {- hsc3 -}

import Sound.DF {- hdf -}

k_sr :: Num n => n
k_sr = 48000

main :: IO ()
main = audition_rju analog_bubbles_buf_msg =<< analog_bubbles_buf_m

analog_bubbles_buf_msg :: [Message]
analog_bubbles_buf_msg = [SC3.b_alloc 0 k_sr 1,SC3.b_alloc 1 k_sr 1]

-- > drawM analog_bubbles_buf_m
-- > analog_bubbles_buf_m >>= audition analog_bubbles_buf_msg
analog_bubbles_buf_m :: (Functor m,UId m) => m (DF ())
analog_bubbles_buf_m = do
  let dpl f a b = liftM2 (,) (f a) (f b)
      mk_o f = fmap (mul_add 3.0 80.0) (lf_saw_m f 0.0)
      mk_f a = fmap (mul_add 24.0 a) (lf_saw_m 0.4 0.0)
      mk_s f = fmap (* 0.04) (sin_osc_m (midi_cps f) 0.0)
  (o1,o2) <- dpl mk_o 8.0 7.23
  (f1,f2) <- dpl mk_f o1 o2
  (s1,s2) <- dpl mk_s f1 f2
  c1 <- buf_comb_n_m 0 s1 0.2 4.0
  c2 <- buf_comb_n_m 1 s2 0.2 4.0
  return (out2 0 c1 c2)
