import Sound.OSC {- hosc -}
import qualified Sound.SC3 as SC3 {- hsc3 -}

import Sound.DF {- hdf -}

main :: IO ()
main = audition_rju analog_bubbles_buf_msg analog_bubbles_buf

analog_bubbles_buf_msg :: [Message]
analog_bubbles_buf_msg = [SC3.b_alloc 0 44100 1,SC3.b_alloc 1 44100 1]

-- > draw analog_bubbles_buf
analog_bubbles_buf :: DF ()
analog_bubbles_buf =
  let dpl f a b = (f a,f b)
      mk_o f = lf_saw f 0.0 * 3.0 + 80.0
      mk_f a = lf_saw 0.4 0.0 * 24.0 + a
      mk_s f = sin_osc (midi_cps f) 0.0 * 0.04
      (o1,o2) = dpl mk_o 8.0 7.23
      (f1,f2) = dpl mk_f o1 o2
      (s1,s2) = dpl mk_s f1 f2
      c1 = buf_comb_n 0 s1 0.2 4.0
      c2 = buf_comb_n 1 s2 0.2 4.0
  in out2 0 c1 c2
