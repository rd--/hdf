import Control.Monad {- base -}

import Sound.OSC {- hosc -}
import qualified Sound.SC3 as SC3 {- hsc3 -}

import Sound.DF {- hdf -}

k_sec = k_seconds_to_frames 48000

-- > drawM lfo_modulation_m
lfo_modulation_m :: (Functor m,UId m) => m (DF ())
lfo_modulation_m = do
  let dpl f p q = liftM2 (,) (f p) (f q)
      mk_p f = fmap (mul_add 3600.0 4000.0) (sin_osc_m f 0.0)
  s <- fmap (mul_add 80.0 160.0) (sin_osc_m 0.05 0.0)
  (p1, p2) <- dpl mk_p 0.6 0.7
  l <- fmap (* 0.05) (lf_pulse_m s 0 0.4)
  (r1, r2) <- dpl (\x -> rlpf_m l x 0.2) p1 p2
  c1 <- comb_n_m (k_sec 0.20) r1 0.20 2.0
  c2 <- comb_n_m (k_sec 0.25) r2 0.25 2.0
  return (out2 0 c1 c2)

main :: IO ()
main = audition_rju [] =<< lfo_modulation_m

lfo_modulation_buf_msg :: [Message]
lfo_modulation_buf_msg = [SC3.b_alloc 0 44100 1, SC3.b_alloc 1 44100 1]

-- > drawM lfo_modulation_buf_m
-- > lfo_modulation_buf_m >>= audition lfo_modulation_buf_msg
lfo_modulation_buf_m :: (Functor m,UId m) => m (DF ())
lfo_modulation_buf_m = do
  let dpl f p q = liftM2 (,) (f p) (f q)
      mk_p f = fmap (mul_add 3600.0 4000.0) (sin_osc_m f 0.0)
  s <- fmap (mul_add 80.0 160.0) (sin_osc_m 0.05 0.0)
  (p1, p2) <- dpl mk_p 0.6 0.7
  l <- fmap (* 0.05) (lf_pulse_m s 0 0.4)
  (r1, r2) <- dpl (\x -> rlpf_m l x 0.2) p1 p2
  c1 <- buf_comb_n_m 0 r1 0.20 2.0
  c2 <- buf_comb_n_m 1 r2 0.25 2.0
  return (out2 0 c1 c2)
