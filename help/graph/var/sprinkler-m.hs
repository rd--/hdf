import Sound.Df {- hdf -}

-- > drawM sprinkler_m
sprinkler_m :: (Functor m,UId m) => m (Df ())
sprinkler_m = do
  let madd m a = fmap ((+ a) . (* m))
  n <- white_noise_m
  f <- madd 10.0 7.0 (lf_pulse_m 0.09 0.0 0.16)
  t <- fmap (* 0.1) (lf_pulse_m f 0.0 0.25)
  o <- bpz2_m (n * t)
  return (out1 0 o)

main :: IO ()
main = sprinkler_m >>= audition_rju []
