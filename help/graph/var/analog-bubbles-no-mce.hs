import Sound.DF {- hdf -}

k_sec = k_seconds_to_frames 48000

-- > draw analog_bubbles
analog_bubbles :: DF ()
analog_bubbles =
  let bimap f g (a,b) = (f a,g b)
      bimap1 f = bimap f f
      mk_o f = lf_saw f 0.0 * 3.0 + 80.0
      mk_f a = lf_saw 0.4 0.0 * 24.0 + a
      mk_s f = sin_osc (midi_cps f) 0.0 * 0.04
      mk_c k z s = comb_n_P k (k_sec z) s 0.2 4.0
      mk_c' = bimap (\z -> mk_c (V_Id 0) 0.2 z) (\z -> mk_c (V_Id 1) 0.4 z)
      c = mk_c' (bimap1 (mk_s . mk_f . mk_o) (8.0,7.23))
  in uncurry (out2 0) c

main :: IO ()
main = audition_rju [] analog_bubbles
