> import qualified Sound.Sc3 as Sc3 {- hsc3 -}
> import Sound.Df {- hdf -}

Comb used as a resonator. The resonant fundamental is equal to
reciprocal of the delay time.

> k_sr = 48000
> alloc_msg = SC3.b_alloc 0 k_sr 1

> g_01 =
>   let n = white_noise (rng 'α')
>       dt = let f x = lin_exp (x + 2.0) 1.0 2.0 0.0001 0.01
>            in f (lf_saw 0.1 0.0)
>       c = buf_comb_n 0 (n * 0.1) dt 0.2
>   in out1 0 c

Comb used as an echo.

> g_02 =
>   let i = impulse 0.5 0.0
>       n = white_noise (rng 'α')
>       e = decay (i * 0.5) 0.2
>       c = buf_comb_n 0 (e * n) 0.2 3.0
>   in out1 0 c

    audition_rju [alloc_msg] g_01
    audition_rju [alloc_msg] g_02
    audition_rju [] (out1 0 0)
