all:
	echo "hdf"

mk-cmd:
	(cd c ; make all install)

clean:
	rm -f html/*.hs.html
	(cd c ; make clean)

mk-svg:
	(cd dot; make all install)

mk-gr:
	(cd gr; make mk-gr)

push-all:
	r.gitlab-push.sh hdf

indent:
	fourmolu -i Sound

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Sound/Df/Gadt/Text.hs Sound/Df/Gadt/Ugen.hs
