// Data flow
Df : Object {
    var <>id;
	+ {arg other; ^DfPrim.new("df_add", [this, other])}
	- {arg other; ^DfPrim.new("df_sub", [this, other])}
	* {arg other; ^DfPrim.new("df_mul", [this, other])}
	/ {arg other; ^DfPrim.new("df_fdiv", [this, other])}
	% {arg other; ^DfPrim.new("df_fmod", [this, other])}
	** {arg other; ^DfPrim.new("df_pow", [this, other])}
	> {arg other; ^DfPrim.new("df_gt", [this, other])}
	< {arg other; ^DfPrim.new("df_lt", [this, other])}
	>= {arg other; ^DfPrim.new("df_gte", [this, other])}
	<= {arg other; ^DfPrim.new("df_lte", [this, other])}
	== {arg other; ^DfPrim.new("df_eq", [this, other])}
	=== {arg other, epsilon = 0.0000001; ^DfPrim.new("df_eq_epsilon", [epsilon, this, other])} // ~= is not allowed
	&& {arg other; ^DfPrim.new("df_and", [this, other])}
	|| {arg other; ^DfPrim.new("df_or", [this, other])}
	not {^DfPrim.new("df_not", [this])}
	min {arg other; ^DfPrim.new("df_min", [this, other])}
	max {arg other; ^DfPrim.new("df_max", [this, other])}
	negate {^DfPrim.new("df_negate", [this])}
    recip {^DfPrim.new("df_recip", [this])}
	abs {^DfPrim.new("df_abs", [this])}
	exp {^DfPrim.new("df_exp", [this])}
	sqrt {^DfPrim.new("df_sqrt", [this])}
	sin {^DfPrim.new("df_sin", [this])}
	cos {^DfPrim.new("df_cos", [this])}
	tan {^DfPrim.new("df_tan", [this])}
	ceil {^DfPrim.new("df_ceil", [this])}
	floor {^DfPrim.new("df_floor", [this])}
	round {^DfPrim.new("df_round", [this])}
    rand {arg other; ^DfPrim.new("df_frand", [this, other])}
	select2 {arg trueDf, falseDf; ^DfPrim.new("df_select2", [this, trueDf, falseDf])}
    linlin {arg inMin, inMax, outMin, outMax; ^(this - inMin) / (inMax - inMin) * (outMax - outMin) + outMin}
    linexp {arg inMin, inMax, outMin, outMax; ^pow(outMax / outMin, (this - inMin) / (inMax - inMin)) * outMin}
    cpsToIncr {arg sampleRate, cycle; ^(cycle / sampleRate) * this}
    clipRight {arg other; (this >= other).select2(this - other, this)}
    clipRightZero {arg other; (this >= other).select2(this - other, (this < 0).select2(this + other, this))}
    // Backward arcs
    *knot {
        arg y0, f;
        var memId = UniqueID.next;
        var rd = DfMemRd.new(memId, y0);
        var fwdRev = f.value(rd);
        var fwd = fwdRev[0];
        var rev = fwdRev[1];
        var wr = DfMemWr.new(memId, y0, rev);
        fwdRev[0].isArray.if({"Df.knot".error},{^DfMrg.new(fwd, [wr])})
    }
    initId {arg idValue; id = idValue}
    *new {^super.new.initId(UniqueID.next)}
}

// Constant (including arrays)
DfLit : Df {
    var <>k;
    init {arg kValue; k = kValue}
    *new {arg k; ^super.new.init(k)}
}

// Memory read
DfMemRd : Df {
    var <>memId, <>y0;
    init {arg memIdValue, y0Value; memId = memIdValue; y0 = y0Value}
    *new {arg memId, y0; ^super.new.init(memId, y0)}
}

// Memory write
DfMemWr : Df {
    var <>memId, <>y0, <>src;
    init {arg memIdValue, y0Value, srcValue; memId = memIdValue; y0 = y0Value; src = srcValue}
    *new {arg memId, y0, src; ^super.new.init(memId, y0, src)}
}

// Primitive
DfPrim : Df {
    var <>name, <>inputs;
    init {arg nameValue, inputsValue; name = nameValue; inputs = inputsValue}
    *new {arg name, inputs; ^super.new.init(name, inputs)}
    arity {^inputs.size}
}

// Demand
DfDmd : Df {
    var <>k, <>trg, <>src;
    init {arg kValue, trgValue, srcValue; k = kValue; trg = trgValue; src = srcValue}
    *new {arg k, trg, src; ^super.new.init(k, trg, src)}
}

DfMrg : Df {
    var <>lhs, <>rhs;
    init {arg lhsValue, rhsValue; lhs = lhsValue; rhs = rhsValue}
    *new {arg lhs, rhs; ^super.new.init(lhs, rhs)}
}

// Single channel input.
In1 : Df {*new {arg bus; ^DfPrim.new("df_in1", [bus])}}

// Single channel output.
Out1 : Df {*new {arg bus, src; ^DfPrim.new("df_out1", [bus, src])}}

// Two channel output (channels c & c+1).
Out2 : Df {*new {arg bus, lhs, rhs; ^DfPrim.new("df_out2", [bus, lhs, rhs])}}

// Single control input.
Ctl1 : Df {*new {arg bus; ^DfPrim.new("df_ctl1", [bus])}}

// Single sample delay with indicated initial value.
UnitDelay : Df {*new {arg y0, s; ^Df.knot(y0, {arg i; [i,s]})}}

// Signal that is initially 'True' then always 'False'.
UnitTrigger : Df {*new {^UnitDelay.new(1, DfLit.new(0))}}

// Single place finite impulse response filter.
Fir1 : Df {*new {arg z0, f, i; ^f.value(i, UnitDelay.new(z0, i))}}

// Two place finite impulse response filter.
Fir2 : Df {*new {arg f, i; var x1 = Delay1.df(i); ^f.value(i, x1, Delay1.df(x1))}}

// Single place infinite impulse response filter with indicated initial value.
Iir1 : Df {*new {arg y0, f, i; ^Df.knot(y0, {arg y1; var r = f.value(i, y1); [r,r]})}}

// Two place infinite impulse response filter.
Iir2 : Df {*new {arg f, i; ^Df.knot(0, {arg y1; var r = f.value(i, y1, UnitDelay.new(0, y1)); [r,r]})}}

// Ordinary biquad filter section.
Biquad : Df {
    *new {
        arg f, i;
        var knotFunc = {
            arg y1;
            var x1 = Delay1.new(i);
            var x2 = Delay1.new(x1);
            var y2 = Delay1.new(y2);
            var r = f.value(i,x1,x2,y1,y2);
            [r,r];
        };
        ^Df.knot(0, knotFunc);
    }
}

SosCoef : Object {
    var <>a0, <>a1, <>a2, <>b1, <>b2;
    init {arg p1,p2,p3,p4,p5; a0 = p1; a1 = p2; a2 = p3; b1 = p4; b2 = p5}
    *new {arg a0, a1, a2, b1, b2; ^super.new.init(a0, a1, a2, b1, b2)}
}

// Second order filter section
Sos : Df {
    *biquadFunc {
        arg a0, a1, a2, b1, b2;
        ^{arg x, x1, x2, y1, y2; (a0 * x) + (a1 * x1) + (a2 * x2) - (b1 * y1) - (b2 * y2)}
    }
    *new {
        arg i, a0, a1, a2, b1, b2;
        ^Biquad.new(Sos.biquadFunc(a0, a1, a2, b1, b2), i);
    }
}

// Counter from indicated initial value by indicated step.
Counter : Df {*new {arg y0, n; ^UnitDelay.new(y0, Iir1.new(y0, {arg i, j; i + j}, n))}}

// Counter that resets to the initial phase at trigger.
CounterReset : Df {
    *new {
        arg y0, n, trg;
        var f = {arg lhs, rhs; trg.select2(DfLit.new(y0), lhs + rhs)};
        ^Iir1.new(y0, f, n);
    }
}

// Counter from 0 to 1 over duration (in seconds).  Holds end value.
UnitLine : Df {*new {arg d; var c = Counter.new(0, SampleDur / d); ^(c > 1).select2(1,c)}}

// linlin of UnitLine.
+ Line {*new{arg s, e, d; ^UnitLine.new(d).linlin(0, 1, s, e)}}

// linexp of UnitLine.
+ XLine {*new{arg s, e, d; ^UnitLine.new(d).linexp(0, 1, s, e)}}

// Single sample delay.
+ Delay1 {*new {arg x; ^UnitDelay.new(0, x)}}

// Two sample delay.
+ Delay2 {*new {arg x; ^Fir2.new({arg i, j, k; k}, x)}}

// Operating sample rate.
+ SampleRate {*new {^DfPrim.new("df_sample_rate", [])}}

// 'recip' of 'w_sample_rate'
+ SampleDur {*new {^SampleRate.new.recip}}

// Mce collapsing output.
+ Out {
    *new {
        arg bus, src;
        var arrayFunc = {
            src.size.switch(
                1, {Out1.new(bus, src[0])},
                2, {Out2.new(bus, src[0], src[1])},
                {"Out.df".error})};
        ^src.isArray.if(arrayFunc, {Out1.new(bus, src)})
    }
}
