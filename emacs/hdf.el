(require 'haskell)
(require 'comint)
(require 'thingatpt)
(require 'find-lisp)

(defcustom hdf-buffer "*hdf*"
  "*The name of the hdf haskell process buffer."
  :type 'string)

(defvar hdf-interpreter (list "ghci")
  "*The name of the haskell interpreter (default=\"ghci\").")

(defvar hdf-directory nil
  "*The hdf directory (default=nil).")

(defun hdf-chunk-string (n s)
  "Split a string into chunks of 'n' characters."
  (let* ((l (length s))
         (m (min l n))
         (c (substring s 0 m)))
    (if (<= l n)
        (list c)
      (cons c (hdf-chunk-string n (substring s n))))))

(defun hdf-send-string (s)
  "Send string and newline to haskell."
  (if (comint-check-proc hdf-buffer)
      (let ((cs (hdf-chunk-string 64 (concat s "\n"))))
        (mapcar
         (lambda (c) (comint-send-string hdf-buffer c))
         cs))
    (error "no hdf process?")))

(defun hdf-send-layout-block (s)
  "Send string to haskell using ghci layout block notation."
  (hdf-send-string (mapconcat 'identity (list ":{" s ":}") "\n")))

(defun hdf-send-text (str)
  "If text spans multiple lines send using using ghci layout quoting."
  (if (string-match "\n" str)
      (hdf-send-layout-block str)
    (hdf-send-string str)))

(defun hdf-send-text-fn (fn str)
  "Send text with fn prefixed."
  (hdf-send-text (if (string-match "\n" str) (concat fn " $\n" str) (concat fn " $ " str))))

(defun hdf-send-quit ()
  "Send :quit instruction to haskell."
  (interactive)
  (hdf-send-string ":quit"))

(defun hdf-find-files (dir rgx)
  (mapc (lambda (filename)
          (find-file-other-window filename))
        (find-lisp-find-files dir rgx)))

(defun hdf-help ()
  "Lookup up the name at point in the hdf help files."
  (interactive)
  (let ((rgx (concat "^" (thing-at-point 'symbol) "\\.help\\.hdf$")))
    (hdf-find-files (concat hdf-directory "help/") rgx)))

(defun hdf-remove-trailing-newline (s)
  "Delete trailing newlines from string."
  (replace-regexp-in-string "\n\\'" "" s))

(defun hdf-send-current-line ()
  "Send the current line."
  (interactive)
  (hdf-send-text
   (buffer-substring-no-properties
    (line-beginning-position)
    (line-end-position))))

(defun hdf-region-string ()
  "Get current region as string."
  (buffer-substring-no-properties (region-beginning) (region-end)))

(defun hdf-send-region ()
  "Send region text"
  (interactive)
  (hdf-send-text (hdf-region-string)))

(defun hdf-send-region-fn (fn)
  "Send region text with haskell function to be applied."
  (hdf-send-text-fn fn (hdf-region-string)))

(defun hdf-play-region ()
  "Play region at jack-dl."
  (interactive)
  (hdf-send-region-fn (format "Sound.Df.audition_rju [] $ out 0")))

(defun hdf-draw-region ()
  "Draw region."
  (interactive)
  (hdf-send-region-fn "Sound.Df.draw $ out 0"))

(defun hdf-print-region ()
  "Print region."
  (interactive)
  (hdf-send-region-fn (format "Sound.Df.audition_text 10 $ out 0")))

(defun hdf-reset ()
  "Reset (play silence)."
  (interactive)
  (hdf-send-string "Sound.Df.audition_rju [] (Sound.Df.out1 0 0.0)"))

(defun hdf-start-haskell ()
  "Start the hdf haskell process.

If `hdf-interpreter' is not already a subprocess it is
started and a new window is created to display the results of
evaluating hdf expressions.  Input and output is via `hdf-buffer'."
  (interactive)
  (if (comint-check-proc hdf-buffer)
      (hdf-see-haskell)
    (apply
     'make-comint
     "hdf"
     (car hdf-interpreter)
     nil
     (cdr hdf-interpreter))
    (hdf-see-haskell)))

(defun hdf-interrupt-haskell ()
  "Interupt haskell."
  (interactive)
  (interrupt-process hdf-buffer comint-ptyp))

(defcustom hdf-seq-degree 2
  "*Number of scsynth processes to address at -seq operations (default=2)."
  :type 'integer)

(defun hdf-server-status-seq ()
  "Send serverStatus request to haskell."
  (interactive)
  (hdf-send-string
   (format
    "Sound.Sc3.withdfAt_seq (\"%s\",%d) %d Sound.Sc3.serverStatus >>= mapM putStrLn . concat"
    hdf-server-host hdf-server-port hdf-seq-degree)))

(defun hdf-load-file (fn)
  "Load named file as string"
  (with-temp-buffer
    (insert-file-contents fn)
    (buffer-substring-no-properties
       (point-min)
       (point-max))))

(defun hdf-import-standard-modules ()
  "Send standard set of hdf and related module imports to haskell."
  (interactive)
  (mapc
   'hdf-send-string
   (split-string (hdf-load-file (concat hdf-directory "lib/hdf-std-imports.hs")) "\n")))

(defun hdf-set-prompt ()
  "Set ghci prompt to hdf."
  (interactive)
  (hdf-send-string ":set prompt \"hdf> \""))

(defun hdf-see-haskell ()
 "Show haskell output."
 (interactive)
  (if (not (comint-check-proc hdf-buffer))
      (hdf-start-haskell)
   (hdf-set-prompt)
   (hdf-import-standard-modules)
   (delete-other-windows)
   (split-window-vertically)
   (with-current-buffer hdf-buffer
     (let ((window (display-buffer (current-buffer))))
       (goto-char (point-max))
       (save-selected-window
         (set-window-point window (point-max)))))))

(defvar hdf-mode-map nil
  "HDF keymap.")

(defun hdf-mode-keybindings (map)
  "HDF keybindings."
  (define-key map (kbd "C-c >") 'hdf-see-haskell)
  (define-key map (kbd "C-c C-c") 'hdf-send-current-line)
  (define-key map (kbd "C-c C-h") 'hdf-help)
  (define-key map (kbd "C-c C-r") 'hdf-send-region)
  (define-key map (kbd "C-c C-a") 'hdf-play-region)
  (define-key map (kbd "C-c C-g") 'hdf-draw-region)
  (define-key map (kbd "C-c C-p") 'hdf-print-region)
  (define-key map (kbd "C-c C-i") 'hdf-interrupt-haskell)
  (define-key map (kbd "C-c C-k") 'hdf-reset)
  (define-key map (kbd "C-c C-q") 'hdf-send-quit)
  (define-key map (kbd "C-c C-.") 'hdf-stop))

(if hdf-mode-map
    ()
  (let ((map (make-sparse-keymap "HDF")))
    (hdf-mode-keybindings map)
    (setq hdf-mode-map map)))

(define-derived-mode
  hdf-mode
  haskell-mode
  "HDF"
  "Major mode for interacting with an inferior hdf process."
  (turn-on-font-lock))

(add-to-list 'auto-mode-alist '("\\.hdf$" . hdf-mode))

(provide 'hdf)
